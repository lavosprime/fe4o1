#![allow(dead_code)]

use super::{Plain, Sugar, SugarLevel};
use super::nodes::*;

trait Unionable {
    type Union: SugarLevel;
}

type Union<T> = <T as Unionable>::Union;

impl Unionable for (Sugar, Sugar) {
    type Union = Sugar;
}

impl Unionable for (Plain, Sugar) {
    type Union = Sugar;
}

impl Unionable for (Sugar, Plain) {
    type Union = Sugar;
}

impl Unionable for (Plain, Plain) {
    type Union = Plain;
}

impl<A, B, C> Unionable for (A, B, C)
    where A: SugarLevel,
          B: SugarLevel,
          C: SugarLevel,
          (B, C): Unionable,
          Union<(B, C)>: SugarLevel,
          (A, Union<(B, C)>): Unionable,
          Union<(A, Union<(B, C)>)>: SugarLevel {
    type Union = <(A, <(B, C) as Unionable>::Union) as Unionable>::Union;
}



pub fn expr_stmt<L: SugarLevel>(expr: Expression<L>) -> Statement<L> {
    Statement::Exp(expr)
}

pub fn asgn_stmt<L: SugarLevel>(name: String, value: Expression<L>) -> Statement<L> {
    Statement::Asgn(asgn_bare(name, value))
}

pub fn def_stmt<L: SugarLevel>(name: String, value: Expression<L>) -> Statement<L> {
    Statement::Def(Define {
        name: name,
        value: value,
    })
}

pub fn print_stmt(value: Expression<Sugar>) -> Statement<Sugar> {
    Statement::Print(PrintStatement {
        value: value,
    })
}

pub fn error_stmt(message: Expression<Sugar>) -> Statement<Sugar> {
    Statement::Error(ErrorStatement {
        message: message,
    })
}

pub fn tblput_stmt(
    table: Expression<Sugar>,
    field: Expression<Sugar>,
    value: Expression<Sugar>)
-> Statement<Sugar> {
    Statement::Put(TablePutStatement {
        table: table,
        field: field,
        value: value,
    })
}

pub fn if_stmt(c: Expression<Sugar>, t: Program<Sugar>, f: Program<Sugar>) -> Statement<Sugar> {
    Statement::If(IfStatement {
        condition: c,
        true_branch: t,
        false_branch: f,
    })
}

pub fn while_stmt(c: Expression<Sugar>, b: Program<Sugar>) -> Statement<Sugar> {
    Statement::While(WhileStatement {
        condition: c,
        body: b,
    })
}

pub fn for_stmt(n: String, i: Expression<Sugar>, b: Program<Sugar>) -> Statement<Sugar> {
    Statement::For(ForStatement {
        name: n,
        iter: i,
        body: b,
    })
}

pub fn ret_stmt<L: SugarLevel>(v: Expression<L>) -> Statement<L> {
    Statement::Ret(ReturnStatement {
        value: v,
    })
}

pub fn asgn_bare<L: SugarLevel>(name: String, value: Expression<L>) -> Assign<L> {
    Assign {
        name: name,
        value: value,
    }
}


pub fn id_expr<L: SugarLevel>(name: String) -> Expression<L> {
    Expression::Id(name)
}

pub fn null_lit_expr<L: SugarLevel>() -> Expression<L> {
    Expression::Lit(Box::new(Literal::Null))
}

pub fn int_lit_expr<L: SugarLevel>(val: i32) -> Expression<L> {
    Expression::Lit(Box::new(Literal::Num(val)))
}

pub fn str_lit_expr<L: SugarLevel>(val: String) -> Expression<L> {
    Expression::Lit(Box::new(Literal::Str(val)))
}

pub fn tbl_lit_expr<L: SugarLevel>(val: Vec<Assign<L>>) -> Expression<L> {
    Expression::Lit(Box::new(Literal::Tbl(val)))
}

pub fn and_expr(op1: Expression<Sugar>, op2: Expression<Sugar>) -> Expression<Sugar> {
    Expression::And(Box::new(AndExpression {
        op1: op1,
        op2: op2
    }))
}

pub fn or_expr(op1: Expression<Sugar>, op2: Expression<Sugar>) -> Expression<Sugar> {
    Expression::Or(Box::new(OrExpression {
        op1: op1,
        op2: op2
    }))
}

// pub fn eq_expr_conform<L1: SugarLevel, L2: SugarLevel>(
//     op1: Expression<L1>,
//     op2: Expression<L2>)
// -> Expression<Union<(L1, L2)>> where (L1, L2): Unionable {
//     Expression::Eq(Box::new(EqExpression::<Union<(L1, L2)>> {
//         op1: Conform::<Expression<Union<(L1, L2)>>>::conform(op1),
//         op2: op2.conform(),
//     }))
// }

pub fn eq_expr<L: SugarLevel>(op1: Expression<L>, op2: Expression<L>) -> Expression<L> {
    Expression::Eq(Box::new(EqExpression {
        op1: op1,
        op2: op2
    }))
}

pub fn ne_expr<L: SugarLevel>(op1: Expression<L>, op2: Expression<L>) -> Expression<L> {
    Expression::Ne(Box::new(NeExpression {
        op1: op1,
        op2: op2
    }))
}

pub fn gt_expr<L: SugarLevel>(op1: Expression<L>, op2: Expression<L>) -> Expression<L> {
    Expression::Gt(Box::new(GtExpression {
        op1: op1,
        op2: op2
    }))
}

pub fn lt_expr<L: SugarLevel>(op1: Expression<L>, op2: Expression<L>) -> Expression<L> {
    Expression::Lt(Box::new(LtExpression {
        op1: op1,
        op2: op2
    }))
}

pub fn ge_expr<L: SugarLevel>(op1: Expression<L>, op2: Expression<L>) -> Expression<L> {
    Expression::Ge(Box::new(GeExpression {
        op1: op1,
        op2: op2
    }))
}

pub fn le_expr<L: SugarLevel>(op1: Expression<L>, op2: Expression<L>) -> Expression<L> {
    Expression::Le(Box::new(LeExpression {
        op1: op1,
        op2: op2
    }))
}

pub fn in_expr(op1: Expression<Sugar>, op2: Expression<Sugar>) -> Expression<Sugar> {
    Expression::In(Box::new(InExpression {
        op1: op1,
        op2: op2
    }))
}

pub fn add_expr<L: SugarLevel>(op1: Expression<L>, op2: Expression<L>) -> Expression<L> {
    Expression::Add(Box::new(AddExpression {
        op1: op1,
        op2: op2
    }))
}

pub fn sub_expr<L: SugarLevel>(op1: Expression<L>, op2: Expression<L>) -> Expression<L> {
    Expression::Sub(Box::new(SubExpression {
        op1: op1,
        op2: op2
    }))
}

pub fn mul_expr<L: SugarLevel>(op1: Expression<L>, op2: Expression<L>) -> Expression<L> {
    Expression::Mul(Box::new(MulExpression {
        op1: op1,
        op2: op2
    }))
}

pub fn div_expr<L: SugarLevel>(op1: Expression<L>, op2: Expression<L>) -> Expression<L> {
    Expression::Div(Box::new(DivExpression {
        op1: op1,
        op2: op2
    }))
}

pub fn lambda_expr<L: SugarLevel>(args: Vec<String>, body: Vec<Statement<L>>) -> Expression<L> {
    Expression::Lam(Box::new(LambdaExpression {
        args: args,
        body: Program::new(body)
    }))
}

pub fn call_expr<L: SugarLevel>(args: Vec<Expression<L>>, func: Expression<L>) -> Expression<L> {
    Expression::Call(Box::new(CallExpression {
        args: args,
        func: func
    }))
}

pub fn expr_expr<L: SugarLevel>(exp: Expression<L>) -> Expression<L> {
    Expression::Exp(Box::new(exp))
}


pub fn tblget_expr(t: Expression<Sugar>, f: Expression<Sugar>) -> Expression<Sugar> {
    Expression::Get(Box::new(TableGetExpression {
        table: t,
        field: f,
    }))
}

pub fn tbllen_expr(t: Expression<Sugar>) -> Expression<Sugar> {
    Expression::Len(Box::new(TableLenExpression {
        table: t,
    }))
}

pub fn thunk<L: SugarLevel>(stmts: Vec<Statement<L>>) -> Expression<L> {
    lambda_expr(Vec::new(), stmts)
}

pub fn iife<L: SugarLevel>(stmts: Vec<Statement<L>>) -> Expression<L> {
    call_expr(Vec::new(), thunk(stmts))
}

pub fn ite<L: SugarLevel>(cond: Expression<L>, t: Expression<L>, f: Expression<L>) -> Expression<L> {
    call_expr(vec![cond, t, f], id_expr("ite".to_string()))
}
