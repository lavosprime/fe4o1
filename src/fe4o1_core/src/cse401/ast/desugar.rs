use super::{Plain, Sugar};
use super::helpers::*;
use super::nodes::*;

pub trait Desugar: Clone {
    type Desugared;
    fn desugar(&self) -> Self::Desugared;
}

impl<T: Node<Plain>> Desugar for T {
    type Desugared = T;
    fn desugar(&self) -> Self::Desugared {
        warn!("desugaring plain node {:?}", self);
        self.clone() // TODO: redefine method to be smarter?
    }
}

impl<T: Desugar> Desugar for Vec<T> {
    type Desugared = Vec<T::Desugared>;
    fn desugar(&self) -> Self::Desugared {
        self.iter().map(|t| t.desugar()).collect()
    }
}

impl Desugar for String {
    type Desugared = String;
    fn desugar(&self) -> Self::Desugared {
        self.clone()
    }
}

macro_rules! desugar_once {
    ($T: ident where $node: ident become $body: expr) => {
        desugar_once!($T as $T where $node become $body);
    };
    ($T: ident as $D: ident where $node: ident become $body: expr) => {
        impl Desugar for $T<Sugar> {
            type Desugared = $D<Plain>;
            fn desugar(&self) -> Self::Desugared {
                let $node = self;
                $body
            }
        }
    }
}

macro_rules! desugar_enum {
    ($T: ident for $($variant: path,)+) => {
        desugar_enum!($T as $T for $($variant),+ where);
    };
    ($T: ident as $D:ident for $($variant: path,)+) => {
        desugar_enum!($T as $D for $($variant,)+ where);
    };
    ($T: ident for $($variant: path),+ where $($pat: pat => $val: expr,)*) => {
        desugar_enum!($T as $T for $($variant),+ where $($pat => $val,)*);
    };
    ($T: ident as $D:ident for $($variant: path),+ where $($pat: pat => $val: expr,)*) => {
        desugar_once!($T as $D where node become match *node {
            $($pat => $val,)*
            $($variant(ref inner) => inner.desugar(),)+
        });
    };
}

macro_rules! desugar_struct {
    ($T: ident {$($field: ident),+}) => {
        desugar_once!($T where node become {
            $T {
                $($field: node.$field.desugar()),+
            }
        });
    };
    ($T: ident as $D: ident use $variant: ident {$($field: ident),+}) => {
        desugar_once!($T as $D where node become {
            $D::$variant($T {
                $($field: node.$field.desugar()),+
            })
        });
    };
    ($T: ident as $D: ident use box $variant: ident {$($field: ident),+}) => {
        desugar_once!($T as $D where node become {
            $D::$variant(Box::new($T {
                $($field: node.$field.desugar()),+
            }))
        });
    }
}

desugar_once!(Program where prog become Program::new(prog.stmts.desugar()));

desugar_enum!(Statement for
    Statement::Asgn,
    Statement::Def,
    Statement::Print,
    Statement::Error,
    Statement::Put,
    Statement::If,
    Statement::While,
    Statement::For,
    Statement::Ret
    where Statement::Exp(ref inner) => expr_stmt(inner.desugar()),
);

desugar_enum!(Expression for
    Expression::Lit,
    Expression::And,
    Expression::Or,
    Expression::Eq,
    Expression::Ne,
    Expression::Gt,
    Expression::Lt,
    Expression::Ge,
    Expression::Le,
    Expression::In,
    Expression::Add,
    Expression::Sub,
    Expression::Mul,
    Expression::Div,
    Expression::Lam,
    Expression::Call,
    Expression::Exp,
    Expression::Get,
    Expression::Len
    where Expression::Id(ref s) => id_expr(s.clone()),
);

desugar_struct!(Define as Statement use Def {name, value});
desugar_struct!(Assign as Statement use Asgn {name, value});
desugar_struct!(ReturnStatement as Statement use Ret {value});
desugar_struct!(LambdaExpression as Expression use box Lam {args, body});
desugar_struct!(CallExpression as Expression use box Call {args, func});


// An abstraction for simple binary operators
macro_rules! plain_bop {
    ($T: ident, $var: ident) => {
        desugar_struct!($T as Expression use box $var {op1, op2});
    }
}

plain_bop!(EqExpression, Eq);
plain_bop!(NeExpression, Ne);
plain_bop!(GtExpression, Gt);
plain_bop!(LtExpression, Lt);
plain_bop!(GeExpression, Ge);
plain_bop!(LeExpression, Le);
plain_bop!(AddExpression, Add);
plain_bop!(SubExpression, Sub);
plain_bop!(MulExpression, Mul);
plain_bop!(DivExpression, Div);


// Nodes that are actually syntactic sugar:
macro_rules! desugar {
    ($T: ident as $D: ident where $node: ident become $body: expr) => {
        impl Desugar for $T {
            type Desugared = $D<Plain>;
            fn desugar(&self) -> Self::Desugared {
                let $node = self;
                let body: $D<Sugar> = $body;
                body.desugar()
            }
        }
    }
}

desugar_once!(Literal as Expression where lit become match *lit {
    Literal::Null => null_lit_expr(),
    Literal::Num(n) => int_lit_expr(n),
    Literal::Str(ref s) => str_lit_expr(s.clone()),
    Literal::Tbl(ref vals) => {
        if vals.is_empty() {
            tbl_lit_expr(Vec::new())
        } else {
            let id = "#sug";
            let mut stmts = Vec::new();
            stmts.push(def_stmt(
                    id.to_string(),
                    tbl_lit_expr(Vec::new()),
            ));
            for ref val in vals {
                stmts.push(tblput_stmt(
                        id_expr(id.to_string()),
                        str_lit_expr(val.name.clone()),
                        val.value.clone(),
                ).desugar());
            }
            stmts.push(expr_stmt(id_expr(id.to_string())));
            iife(stmts)
        }
    },
});

desugar!(AndExpression as Expression where expr become {
    call_expr(
        Vec::new(),
        ite(
            expr.op1.clone(),
            thunk(
                vec![
                    expr_stmt(ite(
                            expr.op2.clone(),
                            int_lit_expr(1),
                            int_lit_expr(0)
                    ))
                ],
            ),
            thunk(
                vec![
                    expr_stmt(int_lit_expr(0))
                ],
            )
        )
    )
});

desugar!(OrExpression as Expression where expr become {
    call_expr(
        Vec::new(),
        ite(
            expr.op1.clone(),
            thunk(
                vec![
                    expr_stmt(int_lit_expr(1))
                ],
            ),
            thunk(
                vec![
                    expr_stmt(ite(
                        expr.op2.clone(),
                        int_lit_expr(1),
                        int_lit_expr(0)
                    ))
                ]
            )
        )
    )
});

desugar!(InExpression as Expression where expr become {
    call_expr(
        vec![
            expr.op2.clone(),
            expr.op1.clone(),
        ],
        id_expr("tblhas".to_string()),
    )
});

desugar!(TableGetExpression as Expression where expr become {
    call_expr(
        vec![
            expr.table.clone(),
            expr.field.clone(),
        ],
        id_expr("tblget".to_string()),
    )
});

desugar!(TableLenExpression as Expression where expr become {
    call_expr(
        vec![
            expr.table.clone(),
        ],
        id_expr("tbllen".to_string()),
    )
});

desugar!(PrintStatement as Statement where stmt become {
    expr_stmt(call_expr(
        vec![
            stmt.value.clone(),
        ],
        id_expr("print".to_string()),
    ))
});

desugar!(ErrorStatement as Statement where stmt become {
    expr_stmt(call_expr(
        vec![
            stmt.message.clone(),
        ],
        id_expr("error".to_string()),
    ))
});

desugar!(TablePutStatement as Statement where stmt become {
    expr_stmt(call_expr(
        vec![
            stmt.table.clone(),
            stmt.field.clone(),
            stmt.value.clone(),
        ],
        id_expr("tblput".to_string()),
    ))
});

desugar!(IfStatement as Statement where stmt become {
    expr_stmt(call_expr(
        Vec::new(),
        ite(
            stmt.condition.clone(),
            thunk(
                stmt.true_branch.stmts.to_vec(),
            ),
            thunk(
                stmt.false_branch.stmts.to_vec(),
            ),
        )
    ))
});

desugar!(WhileStatement as Statement where stmt become {
    let mut body_u1_call = stmt.body.stmts.to_vec();
    body_u1_call.push(expr_stmt(call_expr(
        Vec::new(),
        id_expr("%u1".to_string())
    )));

    expr_stmt(iife(vec![
        def_stmt(
            "%u1".to_string(),
            thunk(vec![
                if_stmt(
                    stmt.condition.clone(),
                    Program::new(body_u1_call),
                    Program::new(Vec::new())
                ),
            ]),
        ),
        expr_stmt(call_expr(
            Vec::new(),
            id_expr("%u1".to_string())
        ))
    ]))
});

desugar!(ForStatement as Statement where stmt become {
    let mut body_u2_call = stmt.body.stmts.to_vec();
    body_u2_call.push(expr_stmt(call_expr(
        Vec::new(),
        id_expr("%u2".to_string())
    )));

    expr_stmt(call_expr(
        Vec::new(),
        thunk(
            vec![
                def_stmt(
                    "%u1".to_string(),
                    stmt.iter.clone(),
                ),
                def_stmt(
                    "%u2".to_string(),
                    thunk(
                        vec![
                            def_stmt(
                                stmt.name.clone(),
                                call_expr(
                                    Vec::new(),
                                    id_expr("%u1".to_string())
                                )
                            ),
                            if_stmt(
                                ne_expr(
                                    id_expr(stmt.name.clone()),
                                    null_lit_expr()
                                ),
                                Program::new(body_u2_call),
                                Program::new(Vec::new())
                            ),
                        ]
                    )
                ),
                expr_stmt(call_expr(
                    Vec::new(),
                    id_expr("%u2".to_string())
                )),
            ]
        )
    ))
});

