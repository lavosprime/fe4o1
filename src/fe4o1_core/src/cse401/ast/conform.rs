//! We use `Conform` to reinterpret nodes as being at equal or higher sugar
//! levels, like casting for sugar levels. This is so that the helper functions
//! to make nodes can be used in both parsing and desugaring, and if any node
//! needs to be Sugar, the entire tree is forced to be Sugar.
//!
//! We'd like all nodes to be able to conform Sugar -> Sugar and Plain -> Plain
//! by default (where applicable), and only need to worry about implementing
//! Plain -> Sugar conforming ourselves. (Do not implement Sugar -> Plain!)
//!
//! Unfortunately, this doesn't work:
//!
//! ```text
//! impl<L, T: Node<L>> Conform<T> for T {
//!     fn conform(self) -> T {
//!         self
//!     }
//! }
//! ```
//!
//! So we need macros instead: one macro for "plain nodes", that can be
//! either Plain or Sugar, and a macro for "sugar nodes", that can only be
//! Sugar.

use super::{Plain, Sugar, SugarLevel};
use super::nodes::*;

/// `Conform` is shorthand for "can be conceptually reinterpreted-in-place as,"
/// possibly with runtime cost but without breaking program invariants.
pub trait Conform<T> : Clone {
    fn conform(self) -> T;
}

impl<'a, B, A: Conform<B>> Conform<B> for &'a A {
    fn conform(self) -> B {
        self.clone().conform()
    }
}

impl<B, A: Conform<B>> Conform<Box<B>> for Box<A> {
    fn conform(self) -> Box<B> {
        Box::new((*self).conform())
    }
}

impl<B, A: Conform<B>> Conform<Vec<B>> for Vec<A> {
    fn conform(self) -> Vec<B> {
        self.iter().map(|t| t.conform()).collect()
    }
}

/// Implement Conform for a plain node type, with the provided exprssion as the
/// implementation for Sugar -> Plain.
macro_rules! conform_plain {
    ($T: ident, $node: ident, $body: expr) => {
        impl<L: SugarLevel> Conform<$T<L>> for $T<L> {
            fn conform(self) -> $T<L> {
                self
            }
        }
        impl Conform<$T<Sugar>> for $T<Plain> {
            fn conform(self) -> $T<Sugar> {
                let $node = self;
                $body
            }
        }
    }
}

/// Implement Conform for a sugar node type.
macro_rules! conform_sugar {
    ($T: ident, $node: ident, $body: expr) => {
        impl Conform<$T> for $T {
            fn conform(self) -> $T {
                self
            }
        }
    }
}

conform_plain!{Program, prog, Program::new(prog.stmts.conform())}

conform_plain!{Statement, stmt, match stmt {
    Statement::Exp(e)   => Statement::Exp(e.conform()),
    Statement::Asgn(e)  => Statement::Asgn(e.conform()),
    Statement::Def(e)   => Statement::Def(e.conform()),
    Statement::Print(e) => Statement::Print(e.conform()),
    Statement::Error(e) => Statement::Error(e.conform()),
    Statement::Put(e)   => Statement::Put(e.conform()),
    Statement::If(e)    => Statement::If(e.conform()),
    Statement::While(e) => Statement::While(e.conform()),
    Statement::For(e)   => Statement::For(e.conform()),
    Statement::Ret(e)   => Statement::Ret(e.conform()),
}}

conform_plain!{Expression, expr, match expr {
    Expression::Id(s)   => Expression::Id(s),
    Expression::Lit(e)  => Expression::Lit(e.conform()),
    Expression::And(e)  => Expression::And(e.conform()),
    Expression::Or(e)   => Expression::Or(e.conform()),
    Expression::Eq(e)   => Expression::Eq(e.conform()),
    Expression::Ne(e)   => Expression::Ne(e.conform()),
    Expression::Gt(e)   => Expression::Gt(e.conform()),
    Expression::Lt(e)   => Expression::Lt(e.conform()),
    Expression::Ge(e)   => Expression::Ge(e.conform()),
    Expression::Le(e)   => Expression::Le(e.conform()),
    Expression::In(e)   => Expression::In(e.conform()),
    Expression::Add(e)  => Expression::Add(e.conform()),
    Expression::Sub(e)  => Expression::Sub(e.conform()),
    Expression::Mul(e)  => Expression::Mul(e.conform()),
    Expression::Div(e)  => Expression::Div(e.conform()),
    Expression::Lam(e)  => Expression::Lam(e.conform()),
    Expression::Call(e) => Expression::Call(e.conform()),
    Expression::Exp(e)  => Expression::Exp(e.conform()),
    Expression::Get(e)  => Expression::Get(e.conform()),
    Expression::Len(e)  => Expression::Len(e.conform()),
}}

conform_plain!{Literal, lit, match lit {
    Literal::Null   => Literal::Null,
    Literal::Num(n) => Literal::Num(n),
    Literal::Str(s) => Literal::Str(s),
    Literal::Tbl(a) => Literal::Tbl(a.conform()),
}}

conform_plain!{Assign, stmt, Assign {
    name: stmt.name.clone(),
    value: stmt.value.conform(),
}}

conform_plain!{Define, stmt, Define {
    name: stmt.name.clone(),
    value: stmt.value.conform(),
}}

conform_sugar!{PrintStatement, stmt, PrintStatement {
    value: stmt.value.conform(),
}}

conform_sugar!{ErrorStatement, stmt, ErrorStatement {
    message: stmt.message.conform(),
}}

conform_sugar!{TablePutStatement, stmt, TablePutStatement {
    table: stmt.table.conform(),
    field: stmt.field.conform(),
    value: stmt.value.conform(),
}}

conform_sugar!{IfStatement, stmt, IfStatement {
    condition: stmt.condition.conform(),
    true_branch: stmt.true_branch.conform(),
    false_branch: stmt.false_branch.conform(),
}}

conform_sugar!{WhileStatement, stmt, WhileStatement {
    condition: stmt.condition.conform(),
    body: stmt.body.conform(),
}}

conform_sugar!{ForStatement, stmt, ForStatement {
    name: stmt.name.clone(),
    iter: stmt.iter.conform(),
    body: stmt.body.conform(),
}}

conform_plain!{ReturnStatement, stmt, ReturnStatement {
    value: stmt.value.conform(),
}}

conform_sugar!{AndExpression, expr, AndExpression {
    op1: expr.op1.conform(),
    op2: expr.op2.conform(),
}}

conform_sugar!{OrExpression, expr, OrExpression {
    op1: expr.op1.conform(),
    op2: expr.op2.conform(),
}}

conform_plain!{EqExpression, expr, EqExpression {
    op1: expr.op1.conform(),
    op2: expr.op2.conform(),
}}

conform_plain!{NeExpression, expr, NeExpression {
    op1: expr.op1.conform(),
    op2: expr.op2.conform(),
}}

conform_plain!{GtExpression, expr, GtExpression {
    op1: expr.op1.conform(),
    op2: expr.op2.conform(),
}}

conform_plain!{LtExpression, expr, LtExpression {
    op1: expr.op1.conform(),
    op2: expr.op2.conform(),
}}

conform_plain!{GeExpression, expr, GeExpression {
    op1: expr.op1.conform(),
    op2: expr.op2.conform(),
}}

conform_plain!{LeExpression, expr, LeExpression {
    op1: expr.op1.conform(),
    op2: expr.op2.conform(),
}}

conform_sugar!{InExpression, expr, InExpression {
    op1: expr.op1.conform(),
    op2: expr.op2.conform(),
}}

conform_plain!{AddExpression, expr, AddExpression {
    op1: expr.op1.conform(),
    op2: expr.op2.conform(),
}}

conform_plain!{SubExpression, expr, SubExpression {
    op1: expr.op1.conform(),
    op2: expr.op2.conform(),
}}

conform_plain!{MulExpression, expr, MulExpression {
    op1: expr.op1.conform(),
    op2: expr.op2.conform(),
}}

conform_plain!{DivExpression, expr, DivExpression {
    op1: expr.op1.conform(),
    op2: expr.op2.conform(),
}}

conform_plain!{LambdaExpression, expr, LambdaExpression {
    args: expr.args.clone(),
    body: expr.body.conform(),
}}

conform_plain!{CallExpression, expr, CallExpression {
    args: expr.args.conform(),
    func: expr.func.conform(),
}}

conform_sugar!{TableGetExpression, expr, TableGetExpression {
    table: expr.table.conform(),
    field: expr.field.conform(),
}}

conform_sugar!{TableLenExpression, expr, TableLenExpression {
    table: expr.table.conform(),
}}

// conform_sugar!{AndExpression}
