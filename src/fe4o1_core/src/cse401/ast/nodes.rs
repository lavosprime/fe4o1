use std::fmt::Debug;
use std::marker::PhantomData;

use ::cse401::btc::compile::Compile;

use super::{Plain, Sugar, SugarLevel};
use super::desugar::Desugar;

pub trait Node<L: SugarLevel> : Clone + Debug {}
trait PlainNode: Compile + Clone + Debug {}
trait SugarNode: Desugar + Clone + Debug {}
impl<T: PlainNode> Node<Plain> for T {}
impl<T: SugarNode> Node<Sugar> for T {}

#[derive(Clone, Debug)]
pub struct Program<L: SugarLevel> {
    pub stmts: Vec<Statement<L>>,
    _marker: PhantomData<L>,
}
impl Node<Plain> for Program<Plain> {} // can't impl PlainNode w/o Compile
impl SugarNode for Program<Sugar> {}

impl<L: SugarLevel> Program<L> {
    pub fn new(stmts: Vec<Statement<L>>) -> Program<L> {
        Program {
            stmts: stmts,
            _marker: PhantomData,
        }
    }
}

//TODO traits instead of enums?

//TODO return, table stuff
#[derive(Clone, Debug)]
pub enum Statement<L: SugarLevel> {
    Exp(Expression<L>),
    Asgn(Assign<L>),
    Def(Define<L>),
    Print(PrintStatement),
    Error(ErrorStatement),
    Put(TablePutStatement),
    If(IfStatement),
    While(WhileStatement),
    For(ForStatement),
    Ret(ReturnStatement<L>),
}
impl PlainNode for Statement<Plain> {}
impl SugarNode for Statement<Sugar> {}

#[derive(Clone, Debug)]
pub enum Expression<L: SugarLevel> {
    Id(String),
    Lit(Box<Literal<L>>),
    And(Box<AndExpression>),
    Or(Box<OrExpression>),
    Eq(Box<EqExpression<L>>),
    Ne(Box<NeExpression<L>>),
    Gt(Box<GtExpression<L>>),
    Lt(Box<LtExpression<L>>),
    Ge(Box<GeExpression<L>>),
    Le(Box<LeExpression<L>>),
    In(Box<InExpression>),
    Add(Box<AddExpression<L>>),
    Sub(Box<SubExpression<L>>),
    Mul(Box<MulExpression<L>>),
    Div(Box<DivExpression<L>>),
    Lam(Box<LambdaExpression<L>>),
    Call(Box<CallExpression<L>>),
    Exp(Box<Expression<L>>),
    Get(Box<TableGetExpression>),
    Len(Box<TableLenExpression>),
    // ite is a native library function
}
impl PlainNode for Expression<Plain> {}
impl SugarNode for Expression<Sugar> {}

#[derive(Clone, Debug)]
pub enum Literal<L: SugarLevel> {
    Null,
    Num(i32),
    Str(String),
    Tbl(Vec<Assign<L>>),
}
impl PlainNode for Literal<Plain> {}
impl SugarNode for Literal<Sugar> {}

// pub enum BinaryOperation<L: SugarLevel> {
//     And(AndExpression<L>),
//     Or(OrExpression<L>),
//     Eq(EqExpression<L>),
//     Ne(NeExpression<L>),
//     Gt(GtExpression<L>),
//     Lt(LtExpression<L>),
//     Ge(GeExpression<L>),
//     Le(LeExpression<L>),
//     In(InExpression<L>),
//     Add(AddExpression<L>),
//     Sub(SubExpression<L>),
//     Mul(MulExpression<L>),
//     Div(DivExpression<L>),
// }

#[derive(Clone, Debug)]
pub struct Assign<L: SugarLevel> {
    pub name: String,
    pub value: Expression<L>,
}
impl PlainNode for Assign<Plain> {}
impl SugarNode for Assign<Sugar> {}

#[derive(Clone, Debug)]
pub struct Define<L: SugarLevel> {
    pub name: String,
    pub value: Expression<L>,
}
impl PlainNode for Define<Plain> {}
impl SugarNode for Define<Sugar> {}

#[derive(Clone, Debug)]
pub struct PrintStatement {
    pub value: Expression<Sugar>,
}
impl SugarNode for PrintStatement {}

#[derive(Clone, Debug)]
pub struct ErrorStatement {
    pub message: Expression<Sugar>,
}
impl SugarNode for ErrorStatement {}

#[derive(Clone, Debug)]
pub struct ReturnStatement<L :SugarLevel> {
    pub value: Expression<L>,
}
impl SugarNode for ReturnStatement<Sugar> {}

#[derive(Clone, Debug)]
pub struct TablePutStatement {
    pub table: Expression<Sugar>,
    pub field: Expression<Sugar>,
    pub value: Expression<Sugar>,
}
impl SugarNode for TablePutStatement {}

#[derive(Clone, Debug)]
pub struct IfStatement {
    pub condition: Expression<Sugar>,
    pub true_branch: Program<Sugar>,
    pub false_branch: Program<Sugar>,
}
impl SugarNode for IfStatement {}

#[derive(Clone, Debug)]
pub struct WhileStatement {
    pub condition: Expression<Sugar>,
    pub body: Program<Sugar>,
}
impl SugarNode for WhileStatement {}

#[derive(Clone, Debug)]
pub struct ForStatement {
    pub name: String,
    pub iter: Expression<Sugar>,
    pub body: Program<Sugar>,
}
impl SugarNode for ForStatement {}

#[derive(Clone, Debug)]
pub struct AndExpression {
    pub op1: Expression<Sugar>,
    pub op2: Expression<Sugar>,
}
impl SugarNode for AndExpression {}

#[derive(Clone, Debug)]
pub struct OrExpression {
    pub op1: Expression<Sugar>,
    pub op2: Expression<Sugar>,
}
impl SugarNode for OrExpression {}

#[derive(Clone, Debug)]
pub struct EqExpression<L: SugarLevel> {
    pub op1: Expression<L>,
    pub op2: Expression<L>,
}
impl PlainNode for EqExpression<Plain> {}
impl SugarNode for EqExpression<Sugar> {}

#[derive(Clone, Debug)]
pub struct NeExpression<L: SugarLevel> {
    pub op1: Expression<L>,
    pub op2: Expression<L>,
}
impl PlainNode for NeExpression<Plain> {}
impl SugarNode for NeExpression<Sugar> {}

#[derive(Clone, Debug)]
pub struct GtExpression<L: SugarLevel> {
    pub op1: Expression<L>,
    pub op2: Expression<L>,
}
impl PlainNode for GtExpression<Plain> {}
impl SugarNode for GtExpression<Sugar> {}

#[derive(Clone, Debug)]
pub struct LtExpression<L: SugarLevel> {
    pub op1: Expression<L>,
    pub op2: Expression<L>,
}
impl PlainNode for LtExpression<Plain> {}
impl SugarNode for LtExpression<Sugar> {}

#[derive(Clone, Debug)]
pub struct GeExpression<L: SugarLevel> {
    pub op1: Expression<L>,
    pub op2: Expression<L>,
}
impl PlainNode for GeExpression<Plain> {}
impl SugarNode for GeExpression<Sugar> {}

#[derive(Clone, Debug)]
pub struct LeExpression<L: SugarLevel> {
    pub op1: Expression<L>,
    pub op2: Expression<L>,
}
impl PlainNode for LeExpression<Plain> {}
impl SugarNode for LeExpression<Sugar> {}

#[derive(Clone, Debug)]
pub struct InExpression {
    pub op1: Expression<Sugar>,
    pub op2: Expression<Sugar>,
}
impl SugarNode for InExpression {}

#[derive(Clone, Debug)]
pub struct AddExpression<L: SugarLevel> {
    pub op1: Expression<L>,
    pub op2: Expression<L>,
}
impl PlainNode for AddExpression<Plain> {}
impl SugarNode for AddExpression<Sugar> {}

#[derive(Clone, Debug)]
pub struct SubExpression<L: SugarLevel> {
    pub op1: Expression<L>,
    pub op2: Expression<L>,
}
impl PlainNode for SubExpression<Plain> {}
impl SugarNode for SubExpression<Sugar> {}

#[derive(Clone, Debug)]
pub struct MulExpression<L: SugarLevel> {
    pub op1: Expression<L>,
    pub op2: Expression<L>,
}
impl PlainNode for MulExpression<Plain> {}
impl SugarNode for MulExpression<Sugar> {}

#[derive(Clone, Debug)]
pub struct DivExpression<L: SugarLevel> {
    pub op1: Expression<L>,
    pub op2: Expression<L>,
}
impl PlainNode for DivExpression<Plain> {}
impl SugarNode for DivExpression<Sugar> {}

#[derive(Clone, Debug)]
pub struct LambdaExpression<L: SugarLevel> {
    pub args: Vec<String>,
    pub body: Program<L>,
}
impl PlainNode for LambdaExpression<Plain> {}
impl SugarNode for LambdaExpression<Sugar> {}

#[derive(Clone, Debug)]
pub struct CallExpression<L: SugarLevel> {
    pub args: Vec<Expression<L>>,
    pub func: Expression<L>,
}
impl PlainNode for CallExpression<Plain> {}
impl SugarNode for CallExpression<Sugar> {}

#[derive(Clone, Debug)]
pub struct TableGetExpression {
    pub table: Expression<Sugar>,
    pub field: Expression<Sugar>,
}
impl SugarNode for TableGetExpression {}

#[derive(Clone, Debug)]
pub struct TableLenExpression {
    pub table: Expression<Sugar>,
}
impl SugarNode for TableLenExpression {}

