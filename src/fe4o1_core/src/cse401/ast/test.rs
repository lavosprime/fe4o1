use super::parser::parse_Program;
use super::nodes::*;

const HELLO: &'static str = r#"
print "hello world";
"#;

#[test]
fn check_ast() {
    let root = parse_Program(HELLO).unwrap();
    assert_eq!(root.stmts.len(), 1);
    let stmt0 = root.stmts[0].clone();
    let printed = match stmt0 {
        Statement::Print(print) => Some(print.value),
        _ => None,
    };
    assert!(!printed.is_none());
    let literal = match printed {
        Some(Expression::Lit(lit)) => Some(*lit),
        _ => None,
    };
    assert!(!literal.is_none());
    let string = match literal {
        Some(Literal::Str(string)) => Some(string),
        _ => None,
    };
    assert_eq!(string.unwrap(), "hello world");
}
