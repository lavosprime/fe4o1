use std::ops::FnMut;

use super::{BinOpType, Bytecode, Program, Register, RegisterGenerator};
use super::helpers::*;

use ::cse401::ast::Plain;
use ::cse401::ast::nodes;

pub fn compile_block(stmts: &Vec<nodes::Statement<Plain>>) -> Program {
    let mut prog = Program::new();
    {
        let mut generator = RegisterGenerator::new();
        let mut gen = || { generator.gen() };
        let mut btc = &mut prog.btc;
        let mut ret_target = None;
        for ref stmt in stmts {
            let target = gen();
            ret_target = Some(target);
            stmt.compile(&mut btc, &mut gen, &target);
        }
        if ret_target.is_none() {
            let target = gen();
            ret_target = Some(target);
            btc.push(Bytecode::Null { target: target });
        }
        btc.push(Bytecode::Return { value: ret_target.unwrap() });
    }
    prog
}

pub trait Compile {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register);
}

impl Compile for nodes::Statement<Plain> {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register) {
        match *self {
            nodes::Statement::Exp(ref e)  => e.compile(btc, gen, target),
            nodes::Statement::Asgn(ref a) => a.compile(btc, gen, target),
            nodes::Statement::Def(ref d)  => d.compile(btc, gen, target),
            nodes::Statement::Ret(ref r)  => r.compile(btc, gen, target),
            _ => {
                panic!("Attempted to compile Statement that should have been desugared. btc so far: {:?}", btc);
            },
        }
    }
}

impl Compile for nodes::Expression<Plain> {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register) {
        match *self {
            nodes::Expression::Id(ref s)   => btc.push(btc_lookup(*target, s.clone())),
            nodes::Expression::Lit(ref l)  => l.compile(btc, gen, target),
            nodes::Expression::Eq(ref e)   => e.compile(btc, gen, target),
            nodes::Expression::Ne(ref e)   => e.compile(btc, gen, target),
            nodes::Expression::Gt(ref e)   => e.compile(btc, gen, target),
            nodes::Expression::Lt(ref e)   => e.compile(btc, gen, target),
            nodes::Expression::Ge(ref e)   => e.compile(btc, gen, target),
            nodes::Expression::Le(ref e)   => e.compile(btc, gen, target),
            nodes::Expression::Add(ref e)  => e.compile(btc, gen, target),
            nodes::Expression::Sub(ref e)  => e.compile(btc, gen, target),
            nodes::Expression::Mul(ref e)  => e.compile(btc, gen, target),
            nodes::Expression::Div(ref e)  => e.compile(btc, gen, target),
            nodes::Expression::Lam(ref e)  => e.compile(btc, gen, target),
            nodes::Expression::Call(ref e) => e.compile(btc, gen, target),
            nodes::Expression::Exp(ref e)  => e.compile(btc, gen, target),
            _ => {
                panic!("Attempted to compile Expression that should have been desugared. btc so far: {:?}", btc);
            },
        }
    }
}

#[allow(unused_variables)]  // gen isn't used because literals are one instruction
impl Compile for nodes::Literal<Plain> {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register) {
        match *self {
            nodes::Literal::Null       => btc.push(btc_null(*target)),
            nodes::Literal::Num(n)     => btc.push(btc_intlit(*target, n as i32)),
            nodes::Literal::Str(ref s) => btc.push(btc_strlit(*target, s.clone())),
            nodes::Literal::Tbl(_) => btc.push(btc_table(*target)),
        }
    }
}

impl Compile for nodes::Assign<Plain> {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register) {
        let value_reg = gen();
        self.value.compile(btc, gen, &value_reg);
        btc.push(btc_assign(*target, self.name.clone(), value_reg));
    }
}

impl Compile for nodes::Define<Plain> {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register) {
        let value_reg = gen();
        self.value.compile(btc, gen, &value_reg);
        btc.push(btc_define(*target, self.name.clone(), value_reg));
    }
}

#[allow(unused_variables)]  // gen isn't used because returns are one instruction
impl Compile for nodes::ReturnStatement<Plain> {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register) {
        btc.push(btc_return(*target));
    }
}

impl Compile for nodes::EqExpression<Plain> {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register) {
        let op1_reg = gen();
        self.op1.compile(btc, gen, &op1_reg);
        let op2_reg = gen();
        self.op2.compile(btc, gen, &op2_reg);
        btc.push(btc_binop(*target, BinOpType::Eq, op1_reg, op2_reg));
    }
}

impl Compile for nodes::NeExpression<Plain> {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register) {
        let op1_reg = gen();
        self.op1.compile(btc, gen, &op1_reg);
        let op2_reg = gen();
        self.op2.compile(btc, gen, &op2_reg);
        btc.push(btc_binop(*target, BinOpType::Ne, op1_reg, op2_reg));
    }
}

impl Compile for nodes::GtExpression<Plain> {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register) {
        let op1_reg = gen();
        self.op1.compile(btc, gen, &op1_reg);
        let op2_reg = gen();
        self.op2.compile(btc, gen, &op2_reg);
        btc.push(btc_binop(*target, BinOpType::Gt, op1_reg, op2_reg))
    }
}

impl Compile for nodes::LtExpression<Plain> {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register) {
        let op1_reg = gen();
        self.op1.compile(btc, gen, &op1_reg);
        let op2_reg = gen();
        self.op2.compile(btc, gen, &op2_reg);
        btc.push(btc_binop(*target, BinOpType::Lt, op1_reg, op2_reg));
    }
}

impl Compile for nodes::GeExpression<Plain> {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register) {
        let op1_reg = gen();
        self.op1.compile(btc, gen, &op1_reg);
        let op2_reg = gen();
        self.op2.compile(btc, gen, &op2_reg);
        btc.push(btc_binop(*target, BinOpType::Ge, op1_reg, op2_reg));
    }
}

impl Compile for nodes::LeExpression<Plain> {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register) {
        let op1_reg = gen();
        self.op1.compile(btc, gen, &op1_reg);
        let op2_reg = gen();
        self.op2.compile(btc, gen, &op2_reg);
        btc.push(btc_binop(*target, BinOpType::Le, op1_reg, op2_reg));
    }
}

impl Compile for nodes::AddExpression<Plain> {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register) {
        let op1_reg = gen();
        self.op1.compile(btc, gen, &op1_reg);
        let op2_reg = gen();
        self.op2.compile(btc, gen, &op2_reg);
        btc.push(btc_binop(*target, BinOpType::Add, op1_reg, op2_reg));
    }
}

impl Compile for nodes::SubExpression<Plain> {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register) {
        let op1_reg = gen();
        self.op1.compile(btc, gen, &op1_reg);
        let op2_reg = gen();
        self.op2.compile(btc, gen, &op2_reg);
        btc.push(btc_binop(*target, BinOpType::Sub, op1_reg, op2_reg));
    }
}

impl Compile for nodes::MulExpression<Plain> {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register) {
        let op1_reg = gen();
        self.op1.compile(btc, gen, &op1_reg);
        let op2_reg = gen();
        self.op2.compile(btc, gen, &op2_reg);
        btc.push(btc_binop(*target, BinOpType::Mul, op1_reg, op2_reg));
    }
}

impl Compile for nodes::DivExpression<Plain> {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register) {
        let op1_reg = gen();
        self.op1.compile(btc, gen, &op1_reg);
        let op2_reg = gen();
        self.op2.compile(btc, gen, &op2_reg);
        btc.push(btc_binop(*target, BinOpType::Div, op1_reg, op2_reg));
    }
}

#[allow(unused_variables)]  //  gen isn't used because lambda bodies use new generators
impl Compile for nodes::LambdaExpression<Plain> {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register) {
        let body_btc_nodes = compile_block(&self.body.stmts);
        btc.push(btc_lambda(*target, self.args.clone(), body_btc_nodes));
    }
}

impl Compile for nodes::CallExpression<Plain> {
    fn compile(&self, btc: &mut Vec<Bytecode>, gen: &mut FnMut() -> Register, target: &Register) {
        let fn_reg = gen();
        self.func.compile(btc, gen, &fn_reg);

        let comp_args = self.args.iter().map(|ref arg: &nodes::Expression<Plain>| -> Register {
            let arg_reg = gen();
            arg.compile(btc, gen, &arg_reg);
            arg_reg
        }).collect::<Vec<Register>>();

        btc.push(btc_call(*target, comp_args, fn_reg));
    }
}


