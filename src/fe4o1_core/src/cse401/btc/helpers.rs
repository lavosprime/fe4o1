use super::{BinOpType, Bytecode, Program, Register};

pub fn btc_binop(target: Register,
                 operator: BinOpType,
                 operand1: Register,
                 operand2: Register) -> Bytecode {
    Bytecode::BinOp {
        target: target,
        operator: operator,
        operand1: operand1,
        operand2: operand2
    }
}

pub fn btc_intlit(target: Register, value: i32) -> Bytecode {
    Bytecode::IntLit {
        target: target,
        value: value,
    }
}

pub fn btc_strlit(target: Register, value: String) -> Bytecode {
    Bytecode::StrLit {
        target: target,
        value: value,
    }
}

pub fn btc_null(target: Register) -> Bytecode {
    Bytecode::Null {
        target: target,
    }
}

pub fn btc_table(target: Register) -> Bytecode {
    Bytecode::Table {
        target: target,
    }
}

pub fn btc_lookup(target: Register, name: String) -> Bytecode {
    Bytecode::Lookup {
        target: target,
        name: name,
    }
}

pub fn btc_define(target: Register, name: String, value: Register) -> Bytecode {
    Bytecode::Define {
        target: target,
        name: name,
        value: value,
    }
}

pub fn btc_assign(target: Register, name: String, value: Register) -> Bytecode {
    Bytecode::Assign {
        target: target,
        name: name,
        value: value,
    }
}

pub fn btc_lambda(target: Register, arguments: Vec<String>, body: Program) -> Bytecode {
    Bytecode::Lambda {
        target: target,
        arguments: arguments,
        body: body,
    }
}

pub fn btc_call(target: Register, arguments: Vec<Register>, function: Register) -> Bytecode {
    Bytecode::Call {
        target: target,
        arguments: arguments,
        function: function,
    }
}

pub fn btc_return(value: Register) -> Bytecode {
    Bytecode::Return {
        value: value,
    }
}
