use std::fmt;

use ::cse401::ast::{Ast, Plain};

pub mod compile;
mod helpers;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct Register(usize);

impl Register {
    pub fn index(&self) -> usize {
        self.0
    }
}

pub struct RegisterGenerator {
    next: usize,
}

impl RegisterGenerator {
    pub fn new() -> Self {
        RegisterGenerator {
            next: 0,
        }
    }

    pub fn gen(&mut self) -> Register {
        let idx = self.next;
        self.next += 1;
        Register(idx)
    }
}

#[derive(Debug)]
pub enum BinOpType {
    Eq,
    Ne,
    Gt,
    Lt,
    Ge,
    Le,
    Add,
    Sub,
    Mul,
    Div,
}

#[derive(Debug)]
pub enum Bytecode {
    BinOp {
        target: Register,
        operator: BinOpType,
        operand1: Register,
        operand2: Register,
    },
    IntLit {
        target: Register,
        value: i32,
    },
    StrLit {
        target: Register,
        value: String,
    },
    Null {
        target: Register,
    },
    Table {
        target: Register,
    },
    Lookup {
        target: Register,
        name: String,
    },
    Define {
        target: Register,
        name: String, // TODO maybe change from string
        value: Register,
    },
    Assign {
        target: Register,
        name: String, // TODO maybe change from string
        value: Register,
    },
    Lambda {
        target: Register,
        arguments: Vec<String>, // TODO maybe change from string
        body: Program,
    },
    Call {
        target: Register,
        arguments: Vec<Register>,
        function: Register,
    },
    Return {
        value: Register,
    },
}

pub struct Program {
    pub btc: Vec<Bytecode>,
}

impl fmt::Debug for Program {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(write!(f, "Program {{ btc: ["));
        for inst in self.btc.iter() {
            match inst {
                &Bytecode::Lambda {
                    ref target,
                    ref arguments,
                    ref body,
                } => try!(write!(f,
                    "Lambda: {{ target: {:?}, arguments: {:?}, body: {:p} }}, ",
                    target,
                    arguments,
                    body.btc.as_ptr()
                )),
                _ => try!(write!(f, "{:?}, ", inst)),
            };
        }
        try!(write!(f, "] }}"));
        Ok(())
    }
}

impl Program {
    pub fn new() -> Program {
        Program {
            btc: Vec::new(),
        }
    }
}

pub fn compile(ast: &Ast<Plain>) -> Program {
    compile::compile_block(&ast.root.stmts)
}
