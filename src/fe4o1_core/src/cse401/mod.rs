use std::io;

pub mod ast;
pub mod btc;
pub mod object;
pub mod runtime;

pub fn interpret(file_name: &str) {
    let ast = match ast::parse_file(file_name) {
        Ok(res) => res,
        Err(e) => {
            match e {
                ast::ParseFileError::SyntaxError => println!("SyntaxError"),
                ast::ParseFileError::IoError(ioe) => println!("{:?}", ioe),
            };
            return;
        },
    };
    let desugared = ast.desugar();
    let prog = btc::compile(&desugared);
    let interpreter = runtime::Interpreter::new();
    interpreter.exec(&prog, &mut io::stdout())
}
