use vec_map::VecMap;

use ::cse401::btc::Register;
use ::cse401::object::Object;

pub struct Registers<'rt> (VecMap<Object<'rt>>);

impl<'rt> Registers<'rt> {
    pub fn new() -> Self {
        Registers(VecMap::new())
    }

    pub fn get(&self, reg: &Register) -> &Object<'rt> {
        trace!("getting register {:?}", reg);
        match self.0.get(reg.index()){
            None => {
                panic!("  register was not yet set");
            },
            Some(ref obj) => {
                trace!("  got value {:?}", obj);
                obj
            }
        }
    }

    pub fn set(&mut self, reg: &Register, obj: &Object<'rt>) {
        trace!("setting register {:?}", reg);
        if let Some(old) = self.0.insert(reg.index(), obj.clone()) {
            trace!("  overwrote {:?}", old);
        }
        trace!("  set value {:?}", obj);
    }
}
