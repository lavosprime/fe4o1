use std::collections::HashMap;
use std::io::{self, Write};

use ::cse401::object::{self, Object};
use ::cse401::btc::{Bytecode, Program};

use super::builtins;
use super::environment::Environment;
use super::error::{Error, Result};
use super::register::Registers;

pub type NativeFunctionId<'rt> = &'rt str;
pub type NativeFunction = for<'rt> Fn(
        &mut Runtime<'rt>,
        &[Object<'rt>])
    -> Result<Object<'rt>>;

type NativeFunctionMap<'rt> = HashMap<NativeFunctionId<'rt>, &'rt NativeFunction>;

pub struct Interpreter<'rt> {
    natives: NativeFunctionMap<'rt>,
}

impl<'rt> Interpreter<'rt> {
    pub fn new() -> Self {
        Interpreter { natives: HashMap::new() }
    }

    pub fn bind_native_function(&mut self,
                                id: NativeFunctionId<'rt>,
                                func: &'rt NativeFunction) {
        if builtins::builtin_native_exists(id) {
            panic!("can't rebind builtin native function {}", id);
        }
        if id.starts_with("$") {
            warn!("binding native function with reserved prefix $: {}", id);
        }
        if let Some(_) = self.natives.insert(id, func) {
            warn!("rebound native function {}", id);
        }
    }

    pub fn exec(&self, prog: &Program, out: &mut Write) {
        let mut rt = Runtime::new(prog, out, &self.natives);
        if let Err(e) = rt.exec() {
            writeln!(rt, "Runtime error: {}", e).unwrap();
        }
    }
}

pub struct Runtime<'rt> {
    state: ProgramState<'rt>,
    output: &'rt mut Write,
    natives: &'rt NativeFunctionMap<'rt>,
}

impl<'rt> Write for Runtime<'rt> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.output.write(buf)
    }
    fn flush(&mut self) -> io::Result<()> {
        self.output.flush()
    }
}

impl<'rt> Runtime<'rt> {
    fn new(prog: &'rt Program, output: &'rt mut Write, natives: &'rt NativeFunctionMap<'rt>) -> Self {
        Runtime {
            state: ProgramState::new(&prog.btc),
            output: output,
            natives: natives,
        }
    }

    #[allow(unused_variables)]  // future-proofing res for steppable exec
    fn exec(&mut self) -> Result<Object<'rt>> {
        use ::cse401::btc::Bytecode::*;
        use ::cse401::btc::BinOpType::*;

        loop {
            let (inst, pc) = {
                let frame = try!(self.state.frame());
                let pc = frame.pc;
                (try!(frame.btc.get(pc).ok_or(Error::DeadRuntime)), pc)
            };
            trace!("executing {}@{}: {:?}", self.state.stack.len(), pc, inst);
            let res = match *inst {
                BinOp {
                    ref target,
                    ref operator,
                    ref operand1,
                    ref operand2,
                } => {
                    let frame = &mut try!(self.state.frame());
                    let op1 = frame.regs.get(operand1).clone();
                    let op2 = frame.regs.get(operand2).clone();
                    let res = match *operator {
                        Eq => {
                            Object::new(op1 == op2)
                        },
                        Ne => {
                            Object::new(op1 != op2)
                        },
                        Gt => {
                            match (op1, op2) {
                                (Object::Num(op1), Object::Num(op2)) => {
                                    Object::new(op1 > op2)
                                },
                                _ => return Err(Error::Type("num", "greater than")),
                            }
                        },
                        Lt => {
                            match (op1, op2) {
                                (Object::Num(op1), Object::Num(op2)) => {
                                    Object::new(op1 < op2)
                                },
                                _ => return Err(Error::Type("num", "less than"))
                            }
                        },
                        Ge => {
                            match (op1, op2) {
                                (Object::Num(op1), Object::Num(op2)) => {
                                    Object::new(op1 >= op2)
                                },
                                _ => return Err(Error::Type("num", "greater or equal"))
                            }
                        },
                        Le => {
                            match (op1, op2) {
                                (Object::Num(op1), Object::Num(op2)) => {
                                    Object::new(op1 <= op2)
                                },
                                _ => return Err(Error::Type("num", "less or equal"))
                            }
                        },
                        Add => {
                            match (op1, op2) {
                                (Object::Num(op1), Object::Num(op2)) => {
                                    Object::new(op1 + op2)
                                },
                                (Object::Str(op1), Object::Num(op2))
                                    | (Object::Num(op2), Object::Str(op1)) => {
                                        Object::new(format!("{}{}", op1, op2))
                                    },
                                    (Object::Str(op1), Object::Str(op2)) => {
                                        Object::new(format!("{}{}", op1, op2))
                                    },
                                    _ => return Err(Error::Type("num or str", "add"))
                            }
                        },
                        Sub => {
                            match (op1, op2) {
                                (Object::Num(op1), Object::Num(op2)) => {
                                    Object::new(op1 - op2)
                                },
                                _ => return Err(Error::Type("num", "sub"))
                            }
                        },
                        Mul => {
                            match (op1, op2) {
                                (Object::Num(op1), Object::Num(op2)) => {
                                    Object::new(op1 * op2)
                                },
                                _ => return Err(Error::Type("num", "mul"))
                            }
                        },
                        Div => {
                            match (op1, op2) {
                                (Object::Num(op1), Object::Num(op2)) => {
                                    if op2 == 0 {
                                        return Err(Error::DivByZero)
                                    }
                                    Object::new(op1 / op2)
                                },
                                _ => return Err(Error::Type("int", "div")),
                            }
                        },
                    };
                    frame.regs.set(target, &res);
                    res
                },

                IntLit {
                    ref target,
                    ref value,
                } => {
                    let frame = &mut try!(self.state.frame());
                    let res = Object::new(value);
                    frame.regs.set(target, &res);
                    res
                },

                StrLit {
                    ref target,
                    ref value,
                } => {
                    let frame = &mut try!(self.state.frame());
                    let res = Object::new(value);
                    frame.regs.set(target, &res);
                    res
                },

                Null {
                    ref target,
                } => {
                    let frame = &mut try!(self.state.frame());
                    let null = Object::Null;
                    frame.regs.set(target, &null);
                    null
                },

                Table {
                    ref target,
                } => {
                    let frame = &mut try!(self.state.frame());
                    let res = Object::new(object::Table::new());
                    frame.regs.set(target, &res);
                    res
                },

                Lookup {
                    ref target,
                    ref name,
                } => {
                    let frame = &mut try!(self.state.frame());
                    let res = try!(frame.env.lookup(name));
                    frame.regs.set(target, &res);
                    res
                },

                Define {
                    ref target,
                    ref name,
                    ref value,
                } => {
                    let frame = &mut try!(self.state.frame());
                    let res = frame.regs.get(value).clone();
                    let ret = Object::Null;
                    try!(frame.env.define(name, &res));
                    frame.regs.set(target, &ret);
                    ret
                },

                Assign {
                    ref target,
                    ref name,
                    ref value,
                } => {
                    let frame = &mut try!(self.state.frame());
                    let res = frame.regs.get(value).clone();
                    let ret = Object::Null;
                    try!(frame.env.assign(name, &res));
                    frame.regs.set(target, &ret);
                    ret
                },

                Lambda {
                    ref target,
                    ref arguments,
                    ref body,
                } => {
                    let frame = &mut try!(self.state.frame());
                    let lambda = object::Function::make_closure(&arguments, &body.btc[..], &frame.env);
                    frame.regs.set(target, &lambda);
                    lambda
                },

                Call {
                    ref target,
                    ref arguments,
                    ref function,
                } => {
                    let (func, args) = {
                        let regs = &try!(self.state.frame()).regs;
                        let func = match regs.get(function).as_fun() {
                            Some(f) => f,
                            None => return Err(Error::Type("function", "call")),
                        };
                        let args: Vec<_> = arguments.iter()
                            .map(|ref reg| regs.get(reg).clone())
                            .collect();
                        (func, args)
                    };
                    let ret = try!(self.invoke(&func, &args[..]));
                    try!(self.state.frame()).regs.set(target, &ret);
                    ret
                },

                Return {
                    ref value,
                } => {
                    return self.state.frame().map(|frame| frame.regs.get(value).clone());
                },
            };
            let frame = &mut try!(self.state.frame());
            frame.pc += 1;
        }
    }

    pub fn invoke(&mut self,
                  func: &object::Function<'rt>,
                  args: &[Object<'rt>]) -> Result<Object<'rt>> {
        match func {
            &object::Function::Clo(ref clo) => self.invoke_closure(clo, args),
            &object::Function::Nat(ref id) => self.invoke_native(id.as_ref(), args),
        }
    }

    fn invoke_closure(&mut self,
                      func: &object::Closure<'rt>,
                      args: &[Object<'rt>]) -> Result<Object<'rt>> {
        if args.len() != func.args.len() {
            return Err(Error::BadNumArgs);
        }
        let mut env = func.env.extend();
        for (name, val) in func.args.iter().zip(args.iter()) {
            try!(env.define(name, val));
        }
        self.state.stack.push(StackFrame::new(&env, func.btc));
        let res = self.exec();
        self.state.stack.pop();
        res
    }

    fn invoke_native(&mut self,
                      id: &str,
                      args: &[Object<'rt>]) -> Result<Object<'rt>> {
        let res = if builtins::builtin_native_exists(id) {
            self.state.stack.push(StackFrame::Native);
            builtins::builtin_native_invoke(id, self, args)
        } else {
            let func = try!(
                self.natives.get(id)
                .ok_or(Error::BadNatFunc(id.to_string())));
            self.state.stack.push(StackFrame::Native);
            func(self, args)
        };
        self.state.stack.pop();
        res
    }
}

struct ProgramState<'rt> {
    stack: Vec<StackFrame<'rt>>,
}

impl<'rt> ProgramState<'rt> {
    fn new(main: &'rt [Bytecode]) -> Self {
        ProgramState {
            stack: vec![StackFrame::new(
                &Environment::root(),
                main,
            )],
        }
    }

    fn frame(&mut self) -> Result<&mut VirtualStackFrame<'rt>> {
        let top = try!(self.stack_top());
        match top {
            &mut StackFrame::Virtual(ref mut vsf) => Ok(vsf),
            _ => panic!("Attempted to read a native stack frame"),
        }
    }

    fn stack_top(&mut self) -> Result<&mut StackFrame<'rt>> {
        match self.stack.last_mut() {
            Some(frame) => Ok(frame),
            None => Err(Error::DeadRuntime),
        }
    }
}

enum StackFrame<'rt> {
    Virtual(VirtualStackFrame<'rt>),
    Native,
}

struct VirtualStackFrame<'rt> {
    env: Environment<'rt>,
    btc: &'rt [Bytecode],
    pc: usize,
    regs: Registers<'rt>,
}

impl<'rt> StackFrame<'rt> {
    fn new(env: &Environment<'rt>, btc: &'rt [Bytecode]) -> Self {
        StackFrame::Virtual(VirtualStackFrame {
            env: env.extend(),
            btc: btc,
            pc: 0,
            regs: Registers::new(),
        })
    }
}

