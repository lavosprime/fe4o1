
pub use self::environment::Environment;
pub use self::error::{Error, Result};
pub use self::interpreter::{Interpreter, NativeFunction, NativeFunctionId, Runtime};

pub mod builtins;
mod environment;
mod error;
mod interpreter;
mod register;
