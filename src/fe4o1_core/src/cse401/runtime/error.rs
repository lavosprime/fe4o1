use std::result;
use std::fmt;

pub type Result<T> = result::Result<T, Error>;

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum Error {
    BadNatFunc(String),
    BadNumArgs,
    DeadRuntime,
    InvalidErrorMsg,
    InvalidEnvAsgn(String),
    InvalidEnvDef(String),
    InvalidEnvGet(String),
    InvalidTblGet,
    IteCondNotBool,
    IteCondNotNum,
    DivByZero,
    ReservedIdent(String),
    Type(&'static str, &'static str),   //TODO: more specific variants?
    UserDefined(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::BadNatFunc(ref s) => write!(f,
                "Trying to call non-existent native function: {}", s),
            Error::BadNumArgs => write!(f,
                "Wrong number of arguments"),
            Error::DeadRuntime => write!(f,
                "The runtime is dead and can no longer execute"),
            Error::InvalidErrorMsg => write!(f,
                "Trying to use non-string as error message"),
            Error::InvalidEnvAsgn(ref s) => write!(f,
                "Trying to assign undefined variable: {}", s),
            Error::InvalidEnvDef(ref s) => write!(f,
                "Trying to define defined variable: {}", s),
            Error::InvalidEnvGet(ref s) => write!(f,
                "Trying to access undefined variable: {}", s),
            Error::InvalidTblGet => write!(f,
                "Trying to get nonexistent table key"),
            Error::IteCondNotBool => write!(f,
                "Condition not a boolean"),
            Error::IteCondNotNum => write!(f,
                "Condition not a number"),
            Error::DivByZero => write!(f,
                "Division by zero"),
            Error::ReservedIdent(ref s) => write!(f,
                "Trying to shadow a reserved identifier: {}", s),
            Error::Type(ref s1, ref s2) => write!(f,
                "Trying to use non-{} in operation {}", s1, s2),
            Error::UserDefined(ref s) => write!(f, "{}", s),
        }
    }
}
