use std::cell::RefCell;
use std::collections::HashMap;
use std::hash;
use std::rc::Rc;

use ::cse401::ast::Identifier;
use ::cse401::object::Object;
use ::cse401::runtime::{Error, Result, builtins};

/// A hierarchical symbol table for storing identifier => value mappings in the
/// runtime environment.
// 'rt is the lifetime of the runtime.
#[derive(Clone, Debug, Default)]
pub struct Environment<'rt> {
    raw: Rc<RefCell<RawEnv<'rt>>>,
}

#[derive(Debug, Default)]
struct RawEnv<'rt> {
    map: HashMap<Identifier<'rt>, Object<'rt>>,
    parent: Option<Environment<'rt>>,
}

impl<'rt> Eq for Environment<'rt> {}
impl<'rt> PartialEq for Environment<'rt> {
    fn eq(&self, other: &Self) -> bool {
        self.as_ptr() == other.as_ptr()
    }
}

impl<'rt> hash::Hash for Environment<'rt> {
    fn hash<H: hash::Hasher>(&self, state: &mut H) {
        self.as_ptr().hash(state);
    }
}

impl<'rt> Environment<'rt> {

    fn as_ptr(&self) -> *const RefCell<RawEnv<'rt>> {
        self.raw.as_ref() as *const RefCell<RawEnv<'rt>>
    }

    /// Creates a root environment.
    pub fn root() -> Self {
        Environment {
            raw: Rc::new(RefCell::new(RawEnv {
                map: HashMap::new(),
                parent: None,
            })),
        }
    }

    /// Creates an environment extending the current one.
    pub fn extend(&self) -> Self {
        Environment {
            raw: Rc::new(RefCell::new(RawEnv {
                map: HashMap::new(),
                parent: Some(self.clone()),
            })),
        }
    }

    pub fn define(&mut self, id: Identifier<'rt>, val: &Object<'rt>) -> Result<()> {
        let mut raw = self.raw.borrow_mut();
        if builtins::builtin_object_exists(id) {
            Err(Error::ReservedIdent(id.to_string()))
        } else if raw.map.contains_key(id) {
            Err(Error::InvalidEnvDef(id.to_string()))
        } else {
            match raw.map.insert(id, val.clone()) {
                Some(old) => panic!(
                    "uncaught bad define: env: {:?}, id: {}, val: {:?} old: {:?}",
                    self, id, val, old,
                ),
                None => Ok(()),
            }
        }
    }

    pub fn assign(&mut self, id: Identifier<'rt>, val: &Object<'rt>) -> Result<()> {
        let mut raw = self.raw.borrow_mut();
        if builtins::builtin_object_exists(id) {
            Err(Error::ReservedIdent(id.to_string()))
        } else if raw.map.contains_key(id) {
            match raw.map.insert(id, val.clone()) {
                Some(_) => Ok(()),
                None => panic!(
                    "uncaught bad assign: env: {:?}, id: {}, val: {:?}",
                    self, id, val,
                ),
            }
        } else {
            match &mut raw.parent {
                &mut Some(ref mut parent_env) => parent_env.assign(id, val),
                &mut None => Err(Error::InvalidEnvAsgn(id.to_string())),
            }
        }
    }

    pub fn lookup(&self, id: Identifier<'rt>) -> Result<Object<'rt>> {
        if builtins::builtin_object_exists(id) {
            builtins::builtin_object_lookup(id)
        } else {
            let raw = self.raw.borrow();
            match raw.map.get(id) {
                Some(val) => Ok(val.clone()),
                None => match raw.parent {
                    Some(ref parent) => parent.lookup(id),
                    None => Err(Error::InvalidEnvGet(id.to_string())),
                },
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::Environment;
    use ::cse401::object::Object;

    #[test]
    fn env_eq() {
        let env1 = Environment::root();
        let env2 = Environment::root();
        assert!(env1 != env2);
        let env3 = env1.clone();
        assert!(env1 == env3);
        assert!(env2 != env3);
    }

    #[test]
    fn env_basic() {
        let mut env = Environment::root();
        assert_eq!(env.define("one", &Object::Num(1)), Ok(()));
    }

    #[test]
    fn env_root_def_get_asgn() {
        let mut env = Environment::root();
        assert_eq!(env.define("one", &Object::new(1)), Ok(()));
        assert_eq!(env.define("two", &Object::new(2)), Ok(()));
        assert_eq!(env.lookup("one"), Ok(Object::new(1)));
        assert_eq!(env.lookup("two"), Ok(Object::new(2)));
        assert_eq!(env.assign("one", &Object::new("one")), Ok(()));
        assert_eq!(env.lookup("one"), Ok(Object::new("one")));
        assert_eq!(env.lookup("two"), Ok(Object::new(2)));
    }

    #[test]
    fn env_extend() {
        let mut env1 = Environment::root();
        let mut env2 = env1.extend();
        assert_eq!(env1.define("one", &Object::new(1)), Ok(()));
        assert_eq!(env1.define("two", &Object::new(2)), Ok(()));
        assert_eq!(env2.define("one", &Object::new("one")), Ok(()));
        assert_eq!(env1.lookup("one"), Ok(Object::new(1)));
        assert_eq!(env1.lookup("two"), Ok(Object::new(2)));
        assert_eq!(env2.lookup("one"), Ok(Object::new("one")));
        assert_eq!(env2.lookup("two"), Ok(Object::new(2)));
        assert_eq!(env2.assign("one", &Object::new("uno")), Ok(()));
        assert_eq!(env1.lookup("one"), Ok(Object::new(1)));
        assert_eq!(env1.lookup("two"), Ok(Object::new(2)));
        assert_eq!(env2.lookup("one"), Ok(Object::new("uno")));
        assert_eq!(env2.lookup("two"), Ok(Object::new(2)));
        assert_eq!(env2.assign("two", &Object::new("dos")), Ok(()));
        assert_eq!(env1.lookup("one"), Ok(Object::new(1)));
        assert_eq!(env1.lookup("two"), Ok(Object::new("dos")));
        assert_eq!(env2.lookup("one"), Ok(Object::new("uno")));
        assert_eq!(env2.lookup("two"), Ok(Object::new("dos")));
    }
}
