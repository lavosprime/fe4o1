use std::io::Write;

use ::cse401::object::{self, Object};
use ::cse401::runtime::{Error, NativeFunctionId, Result};
use ::cse401::runtime::interpreter::Runtime;

macro_rules! builtin_natives {
    ($rt: ident, $args: ident, $($name: ident => $body: expr,)+) => {
        pub fn builtin_native_exists<'rt>(id: NativeFunctionId<'rt>) -> bool {
            match id {
                $(concat!("$", stringify!($name)))|+ => true,
                _ => false,
            }
        }
        pub fn builtin_native_invoke<'rt>(
            id: &str,
            $rt: &mut Runtime<'rt>,
            $args: &[Object<'rt>])
        -> Result<Object<'rt>> {
            match id {
                $(concat!("$", stringify!($name)) => $body,)+
                _ => panic!("Attempted to invoke nonexistent builtin native {}", id),
            }
        }
    }
}

macro_rules! builtin_objects {
    ($($name: ident => $body: expr,)+) => {
        pub fn builtin_object_exists<'rt>(id: &'rt str) -> bool {
            match id {
                $(stringify!($name))|+ => true,
                _ => false,
            }
        }
        pub fn builtin_object_lookup<'rt>(
            id: &'rt str)
        -> Result<Object<'rt>> {
            match id {
                $(stringify!($name) => $body,)+
                _ => panic!("Attempted to lookup nonexistent builtin object {}", id),
            }
        }
    }
}

builtin_natives!{rt, args,

    print => {
        for arg in args {
            write!(rt, "{}", arg).unwrap();
        }
        writeln!(rt, "").unwrap();
        Ok(Object::Null)
    },

    error => {
        if args.len() != 1 {
            //TODO: muli-arg error?
            return Err(Error::BadNumArgs);
        }
        match args[0].as_str() {
            None        => Err(Error::InvalidErrorMsg),
            Some(ref s) => Err(Error::UserDefined(format!("message: {}", s)))
        }
    },

    ite => {
        if args.len() != 3 {
            if cfg!(debug) {
                panic!("Incorrect number of arguments for ite");
            }
            return Err(Error::BadNumArgs);
        }
        if args[0].is_null() {
            return Ok(args[2].clone());
        } else {
            match args[0].as_num() {
                None    => Err(Error::IteCondNotNum),
                Some(c) => match c {
                    0 => Ok(args[2].clone()),
                    _ => Ok(args[1].clone()),
                }
            }
        }
    },

    tblget => {
        if args.len() != 2 {
            if cfg!(debug) {
                panic!("Incorrect number of arguments for tblget");
            }
            return Err(Error::BadNumArgs);
        }
        match args[0].as_tbl() {
            None        => Err(Error::Type("table", "get")),
            Some(ref t) => t.get(&args[1])
        }
    },

    tblhas => {
        if args.len() != 2 {
            if cfg!(debug) {
                panic!("Incorrect number of arguments for tblhas");
            }
            return Err(Error::BadNumArgs);
        }
        match args[0].as_tbl() {
            None        => Err(Error::Type("table", "has")),
            Some(ref t) => {
                if t.has(&args[1]) {
                    return Ok(Object::Num(1i32));
                } else {
                    return Ok(Object::Num(0i32));
                }
            },
        }
    },

    tbllen => {
        if args.len() != 1 {
            if cfg!(debug) {
                panic!("Incorrect number of arguments for tbllen");
            }
            return Err(Error::BadNumArgs);
        }
        match args[0].as_tbl() {
            None        => Err(Error::Type("table", "len")),
            Some(ref t) => Ok(Object::Num(t.len() as i32)),
        }
    },

    tblput => {
        if args.len() != 3 {
            if cfg!(debug) {
                panic!("Incorrect number of arguments for tblput");
            }
            return Err(Error::BadNumArgs);
        }
        match args[0].as_tbl() {
            None        => Err(Error::Type("table", "put")),
            Some(ref mut t) => {
                t.put(&args[1], &args[2]);
                Ok(Object::Null)
            },
        }
    },

    is_fun => {
        if args.len() != 1 {
            if cfg!(debug) {
                panic!("Incorrect number of arguments for is_fun");
            }
            return Err(Error::BadNumArgs);
        }
        if args[0].is_fun() {
            return Ok(Object::Num(1i32));
        } else {
            return Ok(Object::Num(0i32));
        }
    },

    is_tbl => {
        if args.len() != 1 {
            if cfg!(debug) {
                panic!("Incorrect number of arguments for is_fun");
            }
            return Err(Error::BadNumArgs);
        }
        if args[0].is_tbl() {
            return Ok(Object::Num(1i32));
        } else {
            return Ok(Object::Num(0i32));
        }
    },

    is_num => {
        if args.len() != 1 {
            if cfg!(debug) {
                panic!("Incorrect number of arguments for is_fun");
            }
            return Err(Error::BadNumArgs);
        }
        if args[0].is_num() {
            return Ok(Object::Num(1i32));
        } else {
            return Ok(Object::Num(0i32));
        }
    },

    is_str => {
        if args.len() != 1 {
            if cfg!(debug) {
                panic!("Incorrect number of arguments for is_fun");
            }
            return Err(Error::BadNumArgs);
        }
        if args[0].is_str() {
            return Ok(Object::Num(1i32));
        } else {
            return Ok(Object::Num(0i32));
        }
    },

    native => {
        if args.len() != 1 {
            if cfg!(debug) {
                panic!("Incorrect number of arguments for native");
            }
            return Err(Error::BadNumArgs);
        }
        match args[0].as_str() {
            None        => Err(Error::Type("native", "str")),
            Some(ref s) => Ok(object::Function::make_native(s.as_ref()))
        }
    },
}

builtin_objects!{

    print => {
        Ok(object::Function::make_native("$print"))
    },

    error => {
        Ok(object::Function::make_native("$error"))
    },

    ite => {
        Ok(object::Function::make_native("$ite"))
    },

    tblget => {
        Ok(object::Function::make_native("$tblget"))
    },

    tblhas => {
        Ok(object::Function::make_native("$tblhas"))
    },

    tbllen => {
        Ok(object::Function::make_native("$tbllen"))
    },

    tblput => {
        Ok(object::Function::make_native("$tblput"))
    },

    is_fun => {
        Ok(object::Function::make_native("$is_fun"))
    },

    is_tbl => {
        Ok(object::Function::make_native("$is_tbl"))
    },

    is_num => {
        Ok(object::Function::make_native("$is_num"))
    },

    is_str => {
        Ok(object::Function::make_native("$is_str"))
    },

    native => {
        Ok(object::Function::make_native("$native"))
    },

    true => {
        Ok(Object::new(1))
    },

    false => {
        Ok(Object::new(0))
    },
}

