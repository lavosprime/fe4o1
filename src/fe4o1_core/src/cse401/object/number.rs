
use super::{Object, ObjectVariant};

/// The runtime representation of a 401 number.
pub type Number = i32;

impl<'rt> From<bool> for Object<'rt> {
    fn from(b: bool) -> Object<'rt> {
        Object::Num(if b { 1 } else { 0 })
    }
}

impl<'rt, 'a> From<&'a bool> for Object<'rt> {
    fn from(b: &'a bool) -> Object<'rt> {
        Object::Num(if *b { 1 } else { 0 })
    }
}

impl<'rt> From<Number> for Object<'rt> {
    fn from(n: Number) -> Object<'rt> {
        Object::Num(n)
    }
}

impl<'rt, 'a> From<&'a Number> for Object<'rt> {
    fn from(n: &'a Number) -> Object<'rt> {
        Object::Num(n.clone())
    }
}

// impl<'rt, T> From<T> for Object<'rt> where T: Into<Number> {
//     fn from(t: T) -> Object<'rt> {
//         Object::Num(t.into())
//     }
// }

impl<'rt> ObjectVariant<'rt> for Number {
    type Rep = Self;

    fn coerce_object(o: &Object<'rt>) -> Option<Self::Rep> {
        match o {
            &Object::Num(ref n) => Some(n.clone()),
            _ => None,
        }
    }
}

