use std::fmt;
use std::hash;

use uuid::Uuid;

use ::cse401::btc::Bytecode;
use ::cse401::runtime::{Environment, Runtime, Result};


use super::{Object, ObjectVariant};

/// The runtime representation of a 401 function.
#[derive(Clone, Eq, Hash, PartialEq)]
pub enum Function<'rt>{
    Clo(Closure<'rt>),
    Nat(String),
}

use self::Function::{Clo, Nat};

impl<'rt> fmt::Display for Function<'rt> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Clo(_) => write!(f, "Lambda"),
            &Nat(ref i) => write!(f, "Native(\"{}\")", i),
        }
    }
}

impl<'rt> fmt::Debug for Function<'rt> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(write!(f, "Function: {{ "));
        try!(match self {
            &Clo(_) => write!(f, "Closure"),
            &Nat(ref i) => write!(f, "Native(\"{}\")", i),
        });
        write!(f, "}}")
    }
}

impl<'rt> From<Function<'rt>> for Object<'rt> {
    fn from(f: Function<'rt>) -> Object<'rt> {
        Object::Fun(f)
    }
}

impl<'rt, 'a> From<&'a Function<'rt>> for Object<'rt> {
    fn from(f: &'a Function<'rt>) -> Object<'rt> {
        Object::Fun(f.clone())
    }
}

impl<'rt> ObjectVariant<'rt> for Function<'rt> {
    type Rep = Self;

    fn coerce_object(o: &Object<'rt>) -> Option<Self::Rep> {
        match o {
            &Object::Fun(ref f) => Some(f.clone()),
            _ => None,
        }
    }
}

impl<'rt> Function<'rt> {
    pub fn call(&self, rt: &mut Runtime<'rt>, args: &[Object<'rt>]) -> Result<Object<'rt>> {
        rt.invoke(self, args)
    }

    /// Creates a closure based off the specified arguments, body, runtime environment
    pub fn make_closure(args: &'rt Vec<String>, btc: &'rt [Bytecode], env: &Environment<'rt>) -> Object<'rt> {
        Object::Fun(Function::Clo(Closure {
            id: Uuid::new_v4(),
            args: &args[..],
            env: env.clone(),
            btc: btc,
        }))
    }

    pub fn make_native(id: &str) -> Object<'rt> {
        Object::Fun(Function::Nat(id.to_string()))
    }
}

// impl<'a, 'rt: 'a> FnMut(RuntimeHandle<'rt>, &'a [Object<'rt>]) for Function<'rt> {
//     extern "rust-call" fn call_mut(&mut self, args: (RuntimeHandle<'rt>, &'a [Object<'rt>])) -> Result<Object<'rt>> {
//         self.call(args);
//     }
// }

// impl<'a, 'rt: 'a> FnOnce(RuntimeHandle<'rt>, &'a [Object<'rt>]) for Function<'rt> {
//     type Output = Result<Object<'rt>>;
//     extern "rust-call" fn call_once(self, args: (RuntimeHandle, &'a [Object<'rt>])) -> Result<Object<'rt>> {
//         self.call(args);
//     }
// }

/// The runtime representation of a closure created from a 401 lambda.
#[derive(Clone, Debug)]
pub struct Closure<'rt> {
    id: Uuid,
    pub args: &'rt [String],  //TODO make Identifier actually work
    pub env: Environment<'rt>,
    pub btc: &'rt [Bytecode],
}

// TODO: don't just expose fields

impl<'rt> Eq for Closure<'rt> {}
impl<'rt> PartialEq for Closure<'rt> {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl<'rt> hash::Hash for Closure<'rt> {
    fn hash<H: hash::Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}
