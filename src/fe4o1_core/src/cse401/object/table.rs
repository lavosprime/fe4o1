use std::cell::RefCell;
use std::collections::HashMap;
use std::fmt;
use std::hash;
use std::rc::Rc;

use ::cse401::runtime::{Error, Result};

use super::{Object, ObjectVariant};

/// The runtime representation of a 401 table.
#[derive(Clone, Default)]
pub struct Table<'rt> {
    raw: Rc<RefCell<RawTable<'rt>>>,
}

#[derive(Debug, Default)]
struct RawTable<'rt> {
    map: HashMap<Object<'rt>, Object<'rt>>,
    count: usize,
}

impl<'rt> Eq for Table<'rt> {}
impl<'rt> PartialEq for Table<'rt> {
    fn eq(&self, other: &Self) -> bool {
        self.as_ptr() == other.as_ptr()
    }
}

impl<'rt> hash::Hash for Table<'rt> {
    fn hash<H: hash::Hasher>(&self, state: &mut H) {
        self.as_ptr().hash(state);
    }
}

fn format_table<'rt>(
        table: &Table<'rt>,
        in_cycle: bool,
        root: &Table<'rt>,
        f: &mut fmt::Formatter)
    -> fmt::Result {
    let write_obj = |obj: &Object<'rt>, f: &mut fmt::Formatter| -> fmt::Result {
        match obj.as_tbl() {
            Some(val) =>
                if &val == table {
                    try!(write!(f, "self"));
                } else if in_cycle {
                    try!(write!(f, "Table"));
                } else if &val == root {
                    try!(format_table(&val, true, root, f));
                } else {
                    try!(format_table(&val, in_cycle, root, f));
                },
            None => try!(write!(f, "{}", obj)),
        }
        Ok(())
	};
    let raw = table.raw.borrow();
    let ref map = raw.map;
	try!(write!(f, "{{"));
	for (i, (key, value)) in map.iter().enumerate() {
		if i != 0 {
            try!(write!(f, ", "));
		}
        try!(write_obj(key, f));
        try!(write!(f, ": "));
        try!(write_obj(value, f));
	}
	try!(write!(f, "}}"));
    Ok(())
}

impl<'rt> fmt::Display for Table<'rt> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        format_table(self, false, self, f)
    }
}

impl<'rt> fmt::Debug for Table<'rt> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(write!(f, "Table "));
        try!(format_table(self, false, self, f));
        Ok(())
    }
}

impl<'rt> From<Table<'rt>> for Object<'rt> {
    fn from(t: Table<'rt>) -> Object<'rt> {
        Object::Tbl(t)
    }
}

impl<'rt, 'a> From<&'a Table<'rt>> for Object<'rt> {
    fn from(t: &'a Table<'rt>) -> Object<'rt> {
        Object::Tbl(t.clone())
    }
}

impl<'rt> ObjectVariant<'rt> for Table<'rt> {
    type Rep = Self;

    fn coerce_object(o: &Object<'rt>) -> Option<Self::Rep> {
        match o {
            &Object::Tbl(ref t) => Some(t.clone()),
            _ => None,
        }
    }
}

impl<'rt> Table<'rt> {

    fn as_ptr(&self) -> *const RefCell<RawTable<'rt>> {
        self.raw.as_ref() as *const RefCell<RawTable<'rt>>
    }

    /// Creates an empty table.
    pub fn new() -> Self {
        Table {
            raw: Rc::new(RefCell::new(RawTable {
                map: HashMap::new(),
                count: 0,
            })),
        }
    }

    /// Returns the value associated with the given key in the table, or a
    /// runtime error if none exists.
    pub fn get(&self, key: &Object<'rt>) -> Result<Object<'rt>> {
        let raw = self.raw.borrow();
        match raw.map.get(key) {
            Some(val) => Ok(val.clone()),
            None => Err(Error::InvalidTblGet),
        }
    }

    /// Associates the given value with the given key in the table.
    pub fn put(&mut self, key: &Object<'rt>, val: &Object<'rt>) {
        let mut raw = self.raw.borrow_mut();
        raw.map.insert(key.clone(), val.clone());
        while raw.map.contains_key(&Object::Num(raw.count as i32)) {
            raw.count += 1;
        }
    }

    /// Returns the number of consecutive numeric keys in the table starting
    /// from 0.
    pub fn len(&self) -> usize {
        self.raw.borrow().count
    }

    /// Returns whether a value is associated with the given key in the table.
    pub fn has(&self, key: &Object<'rt>) -> bool {
        let raw = self.raw.borrow();
        raw.map.contains_key(key)
    }
}

#[cfg(test)]
mod test {
    use super::Table;
    use ::cse401::object::Object;

    #[test]
    fn tbl_self() {
        let mut table = Table::new();
        let key = Object::new(1);
        let table_obj = Object::Tbl(table.clone());
        table.put(&key, &(table_obj.clone()));
        assert_eq!(table.get(&key), Ok(table_obj.clone()));

        match table_obj.as_tbl() {
            Some(val) => {
                assert_eq!(val, table);
                assert!(val == table);
            },
            None => assert!(false),
        }
    }

    #[test]
    fn table_eq() {
        let tbl1 = Table::new();
        let tbl2 = Table::new();
        assert!(tbl1 != tbl2);
        let tbl3 = tbl1.clone();
        assert!(tbl1 == tbl3);
        assert!(tbl2 != tbl3);
        assert!(Object::new(&tbl1) != Object::new(&tbl2));
        assert!(Object::new(&tbl1) == Object::new(&tbl1));
        assert!(Object::new(&tbl1) == Object::new(&tbl3));
        assert!(Object::new(&tbl2) != Object::new(&tbl3));
    }

}
