use std::borrow::Cow;
use std::string::String as StdString;

use super::{Object, ObjectVariant};

/// The runtime representation of a 401 string.
pub type String<'rt> = Cow<'rt, str>;

impl<'rt> From<String<'rt>> for Object<'rt> {
    fn from(s: String<'rt>) -> Object<'rt> {
        Object::Str(s)
    }
}

impl<'rt, 'a> From<&'a String<'rt>> for Object<'rt> {
    fn from(s: &'a String<'rt>) -> Object<'rt> {
        Object::Str(s.clone())
    }
}

impl<'rt> From<StdString> for Object<'rt> {
    fn from(s: StdString) -> Object<'rt> {
        Object::Str(Cow::Owned(s))
    }
}

impl<'rt, 'a> From<&'rt StdString> for Object<'rt> {
    fn from(s: &'rt StdString) -> Object<'rt> {
        Object::Str(Cow::Owned(s.clone()))
    }
}

impl<'rt> From<&'rt str> for Object<'rt> {
    fn from(s: &'rt str) -> Object<'rt> {
        Object::Str(Cow::Borrowed(s))
    }
}

// impl<'rt, T> From<T> for Object<'rt> where T: Into<SmartStr<'rt>> {
//     fn from(t: T) -> Object<'rt> {
//         Object::Str(t.into())
//     }
// }

impl<'rt> ObjectVariant<'rt> for String<'rt> {
    type Rep = Self;

    fn coerce_object(o: &Object<'rt>) -> Option<Self::Rep> {
        match o {
            &Object::Str(ref s) => Some(s.clone()),
            _ => None,
        }
    }
}

