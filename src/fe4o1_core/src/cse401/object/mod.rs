use std::fmt;

pub use self::function::Function;
pub use self::number::Number;
pub use self::string::String;
pub use self::table::Table;
pub use self::function::Closure;

mod function;
mod number;
mod string;
mod table;

/// The runtime representation of all values in a 401 program.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum Object<'rt> {
    Null,
    Num(Number),
    Str(String<'rt>),
    Fun(Function<'rt>),
    Tbl(Table<'rt>),
    // Err(Error),
}

use self::Object::{Null, Num, Str, Fun, Tbl};

impl<'rt> fmt::Display for Object<'rt> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Null => write!(f, "null"),
            &Num(ref o) => write!(f, "{}", o),
            &Str(ref o) => write!(f, "{}", o),
            &Fun(ref o) => write!(f, "{}", o),
            &Tbl(ref o) => write!(f, "{}", o),
        }
    }
}

trait ObjectVariant<'rt> : Into<Object<'rt>> + fmt::Display {
    type Rep;
    fn coerce_object(&Object<'rt>) -> Option<Self::Rep>;
}

impl<'rt> Object<'rt> {
    pub fn new<T: Into<Self>>(t: T) -> Self {
        t.into()
    }

    pub fn is_null(&self) -> bool {
        match self {
            &Null => true,
            _     => false,
        }
    }

    pub fn is_num(&self) -> bool {
        match self {
            &Num(_) => true,
            _       => false,
        }
    }

    pub fn is_str(&self) -> bool {
        match self {
            &Str(_) => true,
            _       => false,
        }
    }

    pub fn is_fun(&self) -> bool {
        match self {
            &Fun(_) => true,
            _       => false,
        }
    }

    pub fn is_tbl(&self) -> bool {
        match self {
            &Tbl(_) => true,
            _       => false,
        }
    }

    pub fn as_num(&self) -> Option<<Number as ObjectVariant<'rt>>::Rep> {
        <Number as ObjectVariant>::coerce_object(self)
    }

    pub fn as_str(&self) -> Option<<String<'rt> as ObjectVariant<'rt>>::Rep> {
        <String as ObjectVariant>::coerce_object(self)
    }

    pub fn as_fun(&self) -> Option<<Function<'rt> as ObjectVariant<'rt>>::Rep> {
        <Function as ObjectVariant>::coerce_object(self)
    }

    pub fn as_tbl(&self) -> Option<<Table<'rt> as ObjectVariant<'rt>>::Rep> {
        <Table as ObjectVariant>::coerce_object(self)
    }
}

