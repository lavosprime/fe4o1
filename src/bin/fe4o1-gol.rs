#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
extern crate env_logger;
extern crate fe4o1_core;
extern crate bit_vec;

fn main() {
    env_logger::init().unwrap();
    let mut engine = engine::AutomatonEngine::new(40, 30);
    engine.use_rule("conway.401");
    engine.run()
}

mod engine {
    use std::io;
    use fe4o1_core::cse401::{ast, btc, runtime};
    use ::grid::AutomatonGrid;

    pub struct AutomatonEngine {
        width: u32,
        height: u32,
        rule: Option<btc::Program>,
    }

    impl AutomatonEngine {

        pub fn new(width: u32, height: u32) -> Self {
            AutomatonEngine {
                width: width,
                height: height,
                rule: None,
            }
        }

        pub fn use_rule(&mut self, rule_file_name: &str) {
            let ast = match ast::parse_file(rule_file_name) {
                Ok(res) => res,
                Err(e) => match e {
                    ast::ParseFileError::SyntaxError =>
                        panic!("SyntaxError in {}", rule_file_name),
                        ast::ParseFileError::IoError(ioe) =>
                            panic!("{:?}", ioe),
                },
            };
            self.rule = Some(btc::compile(&ast.desugar()));
        }

        pub fn run<'c, 'b, 'a>(&self) {
            let rule = self.rule.iter().next()
                .expect("Can't run automaton without a rule");
            let (width, height) = (self.width, self.height);
            let mut grid = AutomatonGrid::glider_gun(width, height);
            loop {
                println!("{}", grid);
                let mut parse_buf: Vec<u8> = Vec::new();
                let mut interpreter = runtime::Interpreter::new();
                interpreter.bind_native_function("main", ::natives::GOL_MAIN);
                {
                    {
                        let mut ctxt = ::natives::CONTEXT.lock().unwrap();
                        *ctxt = Some((grid, parse_buf));
                    }
                    interpreter.exec(rule, &mut io::stdout());
                    {
                        let mut ctxt = ::natives::CONTEXT.lock().unwrap();
                        let (_, _parse_buf) = ctxt.take().unwrap();
                        parse_buf = _parse_buf;
                    }
               }
                let parse_str = String::from_utf8(parse_buf).unwrap();
                grid = AutomatonGrid::parse(parse_str.as_ref(), width, height);
            }
        }
    }
}

mod natives {
    use std::sync::Mutex;
    use fe4o1_core::cse401::object::{self, Object};
    use fe4o1_core::cse401::runtime::{Error, NativeFunction, Result, Runtime};
    use ::grid::AutomatonGrid;

    lazy_static! {
        pub static ref CONTEXT: Mutex<Option<(AutomatonGrid, Vec<u8>)>> = Mutex::new(None);
    }

    pub const GOL_MAIN: &'static NativeFunction = &gol_main;
    pub fn gol_main<'rt>(rt: &mut Runtime<'rt>,
                         args: &[Object<'rt>]) -> Result<Object<'rt>> {
        if args.len() != 1 {
            return Err(Error::BadNumArgs);
        }
        let successor_fn = args[0].as_fun()
            .expect("main must be passed a successor function");
        let mut lock = CONTEXT.lock().unwrap();
        let mut ctxt = lock.iter_mut().next().unwrap();
        let grid = &ctxt.0;
        let new_cells = &mut ctxt.1;
        let (width, height) = (grid.width(), grid.height());
        for y in 0..height as i32 {
            for x in 0..width as i32 {
                let res = try!(successor_fn.call(rt, &[make_cell(grid, x, y)]))
                    .as_num().expect("Successor function must return bool");
                new_cells.push(if res != 0 { b'.' } else { b' ' });
            }
            new_cells.push(b'\n');
        }
        Ok(Object::Null)
    }

    fn make_cell<'rt>(grid: &AutomatonGrid,
                      cx: i32, cy: i32) -> Object<'rt> {
        let mut cell = object::Table::new();
        let mut neighborhood = object::Table::new();
        let mut i = 0;
        for dx in 0..3 {
            for dy in 0..3 {
                let nx = cx + dx - 1;
                let ny = cy + dy - 1;
                if (nx, ny) == (cx, cy) { continue; }
                let mut neighbor = object::Table::new();
                neighbor.put(&Object::new("is_alive"), &Object::new(grid.get(nx, ny)));
                neighborhood.put(&Object::new(i), &Object::new(neighbor));
                i += 1;
            }
        }
        assert_eq!(i, 8);
        cell.put(&Object::new("neighborhood"), &Object::new(neighborhood));
        cell.put(&Object::new("is_alive"), &Object::new(grid.get(cx, cy)));
        Object::new(cell)
    }
}

mod grid {
    use std::fmt;
    use bit_vec::BitVec;

    pub struct AutomatonGrid {
        bits: BitVec,
        width: u32,
        height: u32,
    }

    impl AutomatonGrid {

        fn new(width: u32, height: u32) -> Self {
            AutomatonGrid {
                bits: BitVec::from_elem((width * height) as usize, false),
                width: width,
                height: height,
            }
        }

        pub fn parse(src: &str, width: u32, height: u32) -> Self {
            let mut grid = AutomatonGrid::new(width, height);
            for (y, line) in src.lines().take(height as usize).enumerate() {
                for (x, chr) in line.chars().take(width as usize).enumerate() {
                    grid.set(x as i32, y as i32, chr != ' ');
                }
            }
            grid
        }

        pub fn glider_gun(width: u32, height: u32) -> Self {
            AutomatonGrid::parse(GLIDER_GUN, width, height)
        }

        pub fn width(&self) -> u32 {
            self.width
        }

        pub fn height(&self) -> u32 {
            self.height
        }

        pub fn get(&self, x: i32, y: i32) -> bool {
            self.bits.get(to_single_index(x, y, self.width, self.height)).unwrap()
        }

        fn set(&mut self, x: i32, y: i32, val: bool) {
            self.bits.set(to_single_index(x, y, self.width, self.height), val)
        }
    }

    impl fmt::Display for AutomatonGrid {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            try!(write!(f, "┌"));
            for _ in 0..self.width {
                try!(write!(f, "──"));
            }
            try!(writeln!(f, "┐"));
            for y in 0..self.height as i32 {
                try!(write!(f, "│"));
                for x in 0..self.width as i32 {
                    if self.get(x, y) {
                        try!(write!(f, "██"));
                    } else {
                        try!(write!(f, "  "));
                    }
                }
                try!(writeln!(f, "│"));
            }
            try!(write!(f, "└"));
            for _ in 0..self.width {
                try!(write!(f, "──"));
            }
            try!(writeln!(f, "┘"));
            Ok(())
        }
    }

    fn to_single_index(x: i32, y: i32, width: u32, height: u32) -> usize {
        let wrapped_x = wrapped(x, width);
        let wrapped_y = wrapped(y, height);
        (wrapped_x + width * wrapped_y) as usize
    }

    fn wrapped(idx: i32, range: u32) -> u32 {
        debug_assert!(range < (::std::i32::MAX as u32));
        debug_assert!(range > 0);
        if idx.is_negative() {
            ((idx + ::std::i32::MAX) as u32) % range
        } else {
            (idx as u32) % range
        }
    }

    const GLIDER_GUN: &'static str = r"



                          █
                        █ █
              ██      ██            ██
             █   █    ██            ██
  ██        █     █   ██
  ██        █   █ ██    █ █
            █     █       █
             █   █
              ██

    ";
//  1234567890123456789012345678901234567890

    #[cfg(test)]
    mod test {
        use super::*;

        #[test]
        fn test_wrapped() {
            assert_eq!(wrapped(0i32, 2u32), 0u32);
            assert_eq!(wrapped(2i32, 2u32), 0u32);
            assert_eq!(wrapped(3i32, 2u32), 1u32);
            assert_eq!(wrapped(3i32, 4u32), 3u32);
            assert_eq!(wrapped(-3i32, 2u32), 1u32);
            assert_eq!(wrapped(-3i32, 5u32), 2u32);
        }

        #[test]
        fn test_index() {
            assert_eq!(to_single_index(0i32, 0i32, 4u32, 3u32), 0u32);
            assert_eq!(to_single_index(1i32, 0i32, 4u32, 3u32), 1u32);
            assert_eq!(to_single_index(0i32, 1i32, 4u32, 3u32), 4u32);
            assert_eq!(to_single_index(1i32, 1i32, 4u32, 3u32), 5u32);
            assert_eq!(to_single_index(3i32, 4i32, 3u32, 4u32), 0u32);
            assert_eq!(to_single_index(4i32, 4i32, 3u32, 4u32), 1u32);
            assert_eq!(to_single_index(3i32, 3i32, 3u32, 4u32), 9u32);
            assert_eq!(to_single_index(4i32, 3i32, 3u32, 4u32), 10u32);
        }
    }

}
