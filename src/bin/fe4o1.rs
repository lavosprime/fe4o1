#[macro_use]
extern crate log;
extern crate env_logger;
extern crate fe4o1_core;

use std::env;

use fe4o1_core::cse401;

fn main() {
    env_logger::init().unwrap();
    //TODO better processing
    for file_name in env::args().skip(1) {
        cse401::interpret(file_name.as_ref());
    }
}
