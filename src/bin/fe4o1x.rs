#[macro_use]
extern crate log;
extern crate env_logger;
extern crate fe4o1_core;

use std::env;
use std::io;

use fe4o1_core::cse401::{self, ast, btc, runtime};

mod extensions {
    use fe4o1_core::cse401::object::{self, Object};
    use fe4o1_core::cse401::runtime::{self, Error, Runtime};
    pub type Result<'rt> = runtime::Result<Object<'rt>>;
    pub type Args<'rt> = [Object<'rt>];

    pub fn bind_all(interpreter: &mut runtime::Interpreter) {
        interpreter.bind_native_function("rem", REM);
        interpreter.bind_native_function("to_hex", TO_HEX);
        interpreter.bind_native_function("arity", ARITY);
    }

    const REM: &'static runtime::NativeFunction = &rem;
    pub fn rem<'rt>(_: &mut Runtime<'rt>, args: &Args<'rt>) -> Result<'rt> {
        if args.len() != 2 {
            return Err(Error::BadNumArgs);
        }
        match (args[0].as_num(), args[1].as_num()) {
            (Some(a), Some(b)) => Ok(Object::new(a % b)),
            _ => Err(Error::Type("number", "rem")),
        }
    }

    const TO_HEX: &'static runtime::NativeFunction = &to_hex;
    pub fn to_hex<'rt>(_: &mut Runtime<'rt>, args: &Args<'rt>) -> Result<'rt> {
        if args.len() != 1 {
            return Err(Error::BadNumArgs);
        }
        match args[0] {
            Object::Null => Ok(Object::new(format!("{:#x}", 0))),
            Object::Num(ref n) => Ok(Object::new(format!("{:#x}", n))),
            _ => Err(Error::Type("number", "to_hex")),
        }
    }

    const ARITY: &'static runtime::NativeFunction = &arity;
    pub fn arity<'rt>(_: &mut Runtime<'rt>, args: &Args<'rt>) -> Result<'rt> {
        if args.len() != 1 {
            return Err(Error::BadNumArgs);
        }
        match args[0].as_fun() {
            Some(object::Function::Clo(f)) => Ok(Object::new(f.args.len() as i32)),
            Some(_) => Ok(Object::Null),
            _ => Err(Error::Type("number", "to_hex")),
        }
    }
}

fn main() {
    env_logger::init().unwrap();
    let mut interpreter = runtime::Interpreter::new();
    extensions::bind_all(&mut interpreter);
    //TODO better processing
    let args = env::args().skip(1);
    let num_args = args.len();
    for file_name in args {
        if num_args > 1 {
            println!("========");
            println!("{}", file_name);
            println!("========");
        }
        process(file_name.as_ref(), &interpreter);
    }
}

fn process(file_name: &str, interpreter: &runtime::Interpreter) {
    let ast = match cse401::ast::parse_file(file_name) {
        Ok(res) => res,
        Err(e) => {
            match e {
                ast::ParseFileError::SyntaxError => println!("SyntaxError"),
                ast::ParseFileError::IoError(ioe) => println!("{:?}", ioe),
            };
            return;
        },
    };
    let desugared = ast.desugar();
    let prog = btc::compile(&desugared);
    interpreter.exec(&prog, &mut io::stdout())
}
